

-- Drop tables in order

DROP TABLE IF EXISTS AssetGroupAdLocation;
DROP TABLE IF EXISTS AssetGroupAdSpace;
DROP TABLE IF EXISTS AssetGroup;
DROP TABLE IF EXISTS AssetGroupType;
DROP TABLE IF EXISTS ReservationAdSpace;
DROP TABLE IF EXISTS AdSpace;
DROP TABLE IF EXISTS AdLocation;
DROP TABLE IF EXISTS AdDesignOrder;
DROP TABLE IF EXISTS AdDesign;
DROP TABLE IF EXISTS Reservation;
DROP TABLE IF EXISTS Client;
DROP TABLE IF EXISTS UserAccount;
DROP TABLE IF EXISTS UserRole;
DROP TABLE IF EXISTS Configuration;


-- Use PostGIS extension (must be installed on PostgreSQL!)

DROP EXTENSION IF EXISTS postgis CASCADE;
CREATE EXTENSION postgis;


-- Create tables & indexes

CREATE TABLE UserRole (
                          id SERIAL NOT NULL PRIMARY KEY,
                          authority VARCHAR(255) NOT NULL
);


CREATE TABLE UserAccount (
                             id SERIAL NOT NULL PRIMARY KEY,
                             username VARCHAR(255) NOT NULL UNIQUE,
                             userRoleId INTEGER NOT NULL,
                             password VARCHAR(255) NOT NULL,
                             enabled BOOLEAN NOT NULL,
                             firstName VARCHAR(50) NOT NULL,
                             lastName VARCHAR(50) NOT NULL,
                             createdAt TIMESTAMP NOT NULL,
                             FOREIGN KEY(userroleid) REFERENCES UserRole(id)
);

CREATE INDEX idx_user_account ON  UserAccount (password, enabled);


CREATE TABLE Client (
                        id SERIAL NOT NULL PRIMARY KEY,
                        creatorUserId INTEGER NOT NULL,
                        name VARCHAR(128),
                        contactPersonName VARCHAR(128),
                        contactPhone VARCHAR(20),
                        email VARCHAR(255),
                        enabled BOOLEAN NOT NULL,
                        createdAt TIMESTAMP NOT NULL,
                        comment VARCHAR(3000),
                        FOREIGN KEY(creatorUserId) REFERENCES UserAccount(id)
);

CREATE INDEX idx_client ON  Client (name, contactPhone, email, enabled);


CREATE TABLE Reservation (
                             id SERIAL NOT NULL PRIMARY KEY,
                             clientId INTEGER NOT NULL,
                             creatorUserId INTEGER NOT NULL,
                             campaignName VARCHAR(100) NOT NULL,
                             cancelled BOOLEAN NOT NULL,
                             createdAt TIMESTAMP NOT NULL,
                             approvedAt TIMESTAMP,
                             invoiceNumber VARCHAR(30),
                             comment VARCHAR(3000),
                             FOREIGN KEY(clientId) REFERENCES Client(id),
                             FOREIGN KEY(creatorUserId) REFERENCES UserAccount(id)
);

CREATE INDEX idx_reservation ON Reservation (clientId, creatorUserId);


CREATE TABLE AdDesign (
                          id SERIAL NOT NULL PRIMARY KEY,
                          reservationId INTEGER NOT NULL,
                          imageFileExists BOOLEAN NOT NULL,
                          image BYTEA,   -- VARBINARY
                          imageFileExtension VARCHAR(255),
                          imageFileName VARCHAR(260),
                          imageUploadTime TIMESTAMP,
                          publicId VARCHAR(24),
                          FOREIGN KEY(reservationId) REFERENCES Reservation(id)
);


CREATE TABLE AdDesignOrder (
                               id SERIAL NOT NULL PRIMARY KEY,
                               orderUserId INTEGER NOT NULL,
                               adDesignId INTEGER NOT NULL,
                               orderedAt TIMESTAMP NOT NULL,
                               orderAmount INTEGER NOT NULL,
                               FOREIGN KEY(orderUserId) REFERENCES UserAccount(id),
                               FOREIGN KEY(adDesignId) REFERENCES AdDesign(id)
);

CREATE INDEX idx_ad_design_order ON AdDesignOrder (adDesignId);


CREATE TABLE AssetGroupType (
    name VARCHAR(20) NOT NULL PRIMARY KEY
);


CREATE TABLE AssetGroup (
                            id SERIAL NOT NULL PRIMARY KEY,
                            assetGroupType VARCHAR NOT NULL,
                            name VARCHAR(50) NOT NULL,
                            description VARCHAR(255),
                            FOREIGN KEY(assetGroupType) REFERENCES AssetGroupType(name)
);


CREATE TABLE AdLocation (
                            id SERIAL NOT NULL PRIMARY KEY,
                            number VARCHAR(10) NOT NULL,
                            enabled BOOLEAN NOT NULL,
                            position GEOGRAPHY(POINT,4326),
                            stopName VARCHAR(50),
                            streetName VARCHAR(50),
                            direction CHARACTER,
                            comment VARCHAR(3000)
);

CREATE INDEX idx_ad_location ON  AdLocation (number, position, enabled, stopName, streetName);



CREATE TABLE AdSpace (
                         id SERIAL NOT NULL PRIMARY KEY,
                         adLocationId INTEGER NOT NULL,
                         locator VARCHAR(50) NOT NULL,
                         enabled BOOLEAN NOT NULL,
                         image BYTEA,     -- VARBINARY
                         imageFileExtension VARCHAR(255),
                         imageFileName VARCHAR(260),
                         imageUploadTime TIMESTAMP,
                         comment VARCHAR(3000),
                         FOREIGN KEY(adLocationId) REFERENCES AdLocation(id)
);

CREATE INDEX idx_ad_space ON  AdSpace (locator, enabled);


CREATE TABLE AssetGroupAdLocation (
                                      id SERIAL NOT NULL PRIMARY KEY,
                                      assetGroupId INTEGER NOT NULL,
                                      adLocationId INTEGER NOT NULL,
                                      FOREIGN KEY(assetGroupId) REFERENCES AssetGroup(id),
                                      FOREIGN KEY(adLocationId) REFERENCES AdLocation(id)
);


CREATE TABLE AssetGroupAdSpace (
                                   id SERIAL NOT NULL PRIMARY KEY,
                                   assetGroupId INTEGER NOT NULL,
                                   adSpaceId INTEGER NOT NULL,
                                   FOREIGN KEY(assetGroupId) REFERENCES AssetGroup(id),
                                   FOREIGN KEY(adSpaceId) REFERENCES AdSpace(id)
);


CREATE TABLE ReservationAdSpace (
                                    id SERIAL NOT NULL PRIMARY KEY,
                                    adSpaceId INTEGER NOT NULL,
                                    reservationId INTEGER NOT NULL,
                                    adDesignId INTEGER NOT NULL,
                                    installerUserId INTEGER NOT NULL,
                                    fromTime TIMESTAMP NOT NULL,
                                    toTime TIMESTAMP NOT NULL,
                                    adInstalledAt TIMESTAMP,
                                    FOREIGN KEY(adSpaceId) REFERENCES AdSpace(id),
                                    FOREIGN KEY(reservationId) REFERENCES Reservation(id),
                                    FOREIGN KEY(adDesignId) REFERENCES AdDesign(id),
                                    FOREIGN KEY(installerUserId) REFERENCES UserAccount(id)
);

CREATE INDEX idx_reservation_ad_space ON  ReservationAdSpace (fromTime, toTime, installerUserId, adDesignId, reservationId, adSpaceId, adInstalledAt);



CREATE TABLE Configuration (
                               key VARCHAR(100) UNIQUE NOT NULL,
                               value VARCHAR(2000)
);


