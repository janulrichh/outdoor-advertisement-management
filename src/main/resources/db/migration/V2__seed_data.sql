-- Insert predefined RBAC user roles
INSERT INTO userrole (id, authority) VALUES (1, 'ROLE_ADMIN');
INSERT INTO userrole (id, authority) VALUES (2, 'ROLE_SALES');
INSERT INTO userrole (id, authority) VALUES (3, 'ROLE_INSTALL');

-- Insert predefined AssetGroupTypes
INSERT INTO assetgrouptype (name) VALUES ('AD_LOCATION');
INSERT INTO assetgrouptype (name) VALUES ('AD_SPACE');
INSERT INTO assetgrouptype (name) VALUES ('COMBINED');

-- Insert default dynamic configurations
INSERT INTO configuration (key, value) VALUES ('ORDER_RECIPIENT', 'jansut@taltech.ee');

-- Insert example user accounts
INSERT INTO useraccount (userroleid, username, password, enabled, firstname, lastname, createdat)
VALUES (1, 'admin', '$2y$10$0T1fpM9/x6vKZ5DAvHGq7epm8NWv0hyeDZUfeP0nLIdnyEc97L6Mm', true, 'Admin', 'Person', CURRENT_TIMESTAMP);
INSERT INTO useraccount (userroleid, username, password, enabled, firstname, lastname, createdat)
VALUES (2, 'sales', '$2y$10$0T1fpM9/x6vKZ5DAvHGq7epm8NWv0hyeDZUfeP0nLIdnyEc97L6Mm', true, 'Sales', 'Person', CURRENT_TIMESTAMP);
INSERT INTO useraccount (userroleid, username, password, enabled, firstname, lastname, createdat)
VALUES (3, 'install', '$2y$10$0T1fpM9/x6vKZ5DAvHGq7epm8NWv0hyeDZUfeP0nLIdnyEc97L6Mm', true, 'Installer', 'Person', CURRENT_TIMESTAMP);
