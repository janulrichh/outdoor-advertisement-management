package com.admanagement.app.persistence.repositories;

import com.admanagement.app.persistence.model.AdLocation;
import com.admanagement.app.persistence.model.AdSpace;
import jakarta.transaction.Transactional;
import org.locationtech.jts.geom.Point;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AdLocationRepository extends BaseRepository<AdLocation, Integer> {

    public AdLocationRepository() {
        super(AdLocation.class);
    }

    public List<AdLocation> findInArea(Point center, Double radius) {
        return entityManager.createQuery(
                "SELECT loc FROM AdLocation loc " +
                        "WHERE cast(st_dwithin(loc.position, :center, :radius) AS boolean) " +
                        "AND loc.enabled", AdLocation.class)
                .setParameter("center", center)
                .setParameter("radius", radius)
                .getResultList();
    }

    @Override
    public List<AdLocation> findAll() {
        String jpql = "SELECT l FROM AdLocation l WHERE l.enabled";
        return entityManager.createQuery(jpql, AdLocation.class).getResultList();
    }

    @Override
    @Transactional
    public AdLocation save(AdLocation entity) {
        if (entity.getEnabled() == null)
            entity.setEnabled(true);
        entityManager.persist(entity);
        return entity;
    }

    @Override
    @Transactional
    public AdLocation update(AdLocation entity, Integer id) {
        AdLocation entityToSave = entityManager.find(AdLocation.class, id);
        if (entity.getEnabled() == null)
            entity.setEnabled(entityToSave.getEnabled());
        entityManager.merge(entity);
        return entity;
    }

    @Override
    @Transactional
    public void deleteById(Integer id) {
        AdLocation entityToRemove = entityManager.find(AdLocation.class, id);
        if (entityToRemove != null) {
            List<AdSpace> adSpacesInAdLocation = entityToRemove.getAdSpaces();
            for (AdSpace as : adSpacesInAdLocation) {
                as.setEnabled(false);
                entityManager.persist(as);
            }
            entityToRemove.setEnabled(false);
            entityManager.persist(entityToRemove);
        }
    }
}
