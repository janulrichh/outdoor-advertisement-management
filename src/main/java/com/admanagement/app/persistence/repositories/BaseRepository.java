package com.admanagement.app.persistence.repositories;


import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;

import java.util.List;
import java.util.Optional;


@Transactional
public abstract class BaseRepository<T, ID> implements AbstractRepository<T, ID> {

    @PersistenceContext
    protected EntityManager entityManager;

    private final Class<T> entityClass;

    public BaseRepository(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Transactional
    public T save(T entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Transactional
    public T update(T entity, ID id) {
        T entityToSave = entityManager.find(entityClass, id);
        if (entityToSave != null) {
            entityManager.merge(entity);
            return entity;
        }
        return null;
    }

    public Optional<T> findById(ID id) {
        return Optional.ofNullable(entityManager.find(entityClass, id));
    }

    public List<T> findAll() {
        return entityManager.createQuery("SELECT e FROM " + entityClass.getSimpleName() + " e", entityClass)
                .getResultList();
    }

    public void deleteById(ID id) {
        T entityToRemove = entityManager.find(entityClass, id);
        if (entityToRemove != null) {
            entityManager.remove(entityToRemove);
        }
    }
}
