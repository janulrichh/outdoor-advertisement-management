package com.admanagement.app.persistence.repositories;


import com.admanagement.app.persistence.model.Client;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Transactional
public class ClientRepository extends BaseRepository<Client, Integer> {

    public ClientRepository() {
        super(Client.class);
    }

    @Override
    public List<Client> findAll() {
        String jpql = "SELECT c FROM Client c WHERE c.enabled = true";
        return entityManager.createQuery(jpql, Client.class).getResultList();
    }

    @Override
    @Transactional
    public Client save(Client entity) {
        if (entity.getEnabled() == null)
            entity.setEnabled(true);
        entityManager.persist(entity);
        return entity;
    }

    @Override
    @Transactional
    public Client update(Client entity, Integer id) {
        Client entityToSave = entityManager.find(Client.class, id);
        if (entity.getEnabled() == null)
            entity.setEnabled(entityToSave.getEnabled());
        entity.setCreatedAt(entityToSave.getCreatedAt());
        entityManager.merge(entity);
        return entity;
    }

    @Override
    public void deleteById(Integer id) {
        Client entityToRemove = entityManager.find(Client.class, id);
        if (entityToRemove != null) {
            entityToRemove.setEnabled(false);
            entityManager.persist(entityToRemove);
        }
    }
}
