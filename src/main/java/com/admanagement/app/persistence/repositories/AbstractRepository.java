package com.admanagement.app.persistence.repositories;

import java.util.List;
import java.util.Optional;

public interface AbstractRepository<T, ID> {

    T save(T entity);

    T update(T entity, ID id);

    Optional<T> findById(ID id);

    List<T> findAll();

    void deleteById(ID id);

}
