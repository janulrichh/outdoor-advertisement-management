package com.admanagement.app.persistence.repositories;

import com.admanagement.app.persistence.model.AdDesignOrder;
import org.springframework.stereotype.Repository;

@Repository
public class AdDesignOrderRepository extends BaseRepository<AdDesignOrder, Integer> {

    public AdDesignOrderRepository() {
        super(AdDesignOrder.class);
    }

}
