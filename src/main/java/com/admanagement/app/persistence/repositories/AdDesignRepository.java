package com.admanagement.app.persistence.repositories;

import com.admanagement.app.persistence.model.AdDesign;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


@Repository
public class AdDesignRepository extends BaseRepository<AdDesign, Integer> {

    public AdDesignRepository() {
        super(AdDesign.class);
    }

    public List<AdDesign> findAllWaitingToOrder() {
        return entityManager.createQuery(
            "SELECT d FROM AdDesign d " +
                    "INNER JOIN ReservationAdSpace rs ON rs.adDesign = d " +
                    "LEFT JOIN AdDesignOrder o ON o.adDesign = d " +
                    "WHERE COALESCE((SELECT SUM(o.orderAmount) FROM AdDesignOrder o WHERE o.adDesign = d), 0) " +
                    "< (SELECT COUNT(*) FROM ReservationAdSpace rs WHERE rs.adDesign = d) " +
                    "AND d.imageFileExists", AdDesign.class)
                .getResultList();
    }

    public Optional<AdDesign> findByPublicId(String publicId) {
        return Optional.ofNullable(entityManager.createQuery(
                "SELECT d FROM AdDesign d WHERE d.publicId = :publicId ", AdDesign.class)
                .setParameter("publicId", publicId).getSingleResult());
    }

    public void deleteOldAdDesignImages() {
        entityManager.createQuery(
                    "UPDATE AdDesign d " +
                    "SET image = :image, imageFileExists = false " +
                    "WHERE d IN (SELECT rs.adDesign FROM ReservationAdSpace rs " +
                    "WHERE rs.adDesign = d) " +
                    "AND d NOT IN (SELECT rs.adDesign FROM ReservationAdSpace rs " +
                    "WHERE rs.adDesign = d AND rs.toTime > CURRENT_TIMESTAMP)")
                .setParameter("image", null)
                .executeUpdate();
    }
}
