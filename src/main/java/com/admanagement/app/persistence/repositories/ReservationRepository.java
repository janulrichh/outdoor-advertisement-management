package com.admanagement.app.persistence.repositories;

import com.admanagement.app.persistence.model.AdDesign;
import com.admanagement.app.persistence.model.Reservation;
import com.admanagement.app.persistence.model.ReservationAdSpace;
import jakarta.transaction.Transactional;
import org.locationtech.jts.geom.Point;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class ReservationRepository extends BaseRepository<Reservation, Integer> {

    public ReservationRepository() {
        super(Reservation.class);
    }

    public List<Reservation> findInAreaToInstall(Point center, Double radius) {
        String jpql = "SELECT r FROM Reservation r " +
                "INNER JOIN ReservationAdSpace rs ON rs.reservation = r " +
                "INNER JOIN AdSpace s ON rs.adSpace = s " +
                "INNER JOIN AdLocation l ON s.adLocation = l " +
                "WHERE cast(st_dwithin(l.position, :center, :radius) AS boolean) " +
                "AND rs.fromTime > CURRENT_TIMESTAMP " +
                "AND rs.adInstalledAt IS NULL " +
                "ORDER BY rs.fromTime";
        return entityManager.createQuery(jpql, Reservation.class)
                .setParameter("center", center)
                .setParameter("radius", radius)
                .getResultList();
    }

    public List<Reservation> findAllToInstall() {
        String jpql = "SELECT r FROM Reservation r " +
                "INNER JOIN ReservationAdSpace rs ON rs.reservation = r " +
                "INNER JOIN AdSpace s ON rs.adSpace = s " +
                "INNER JOIN AdLocation l ON s.adLocation = l " +
                "WHERE rs.fromTime > CURRENT_TIMESTAMP " +
                "AND rs.adInstalledAt IS NULL " +
                "ORDER BY rs.fromTime";
        return entityManager.createQuery(jpql, Reservation.class).getResultList();
    }

    public List<Reservation> findInTimeRangeInArea(Point center, Double radius,
                                                   Instant fromTime, Instant toTime) {
        String jpql =
                "SELECT r FROM Reservation r " +
                "INNER JOIN ReservationAdSpace rs ON rs.reservation = r " +
                "INNER JOIN AdSpace s ON rs.adSpace = s " +
                "INNER JOIN AdLocation l ON s.adLocation = l " +
                "WHERE cast(st_dwithin(l.position, :center, :radius) AS boolean) " +
                "AND (rs.fromTime >= :fromTime AND rs.fromTime <= :toTime) " +
                "OR (rs.toTime >= :fromTime AND rs.toTime <= :toTime)";
        return entityManager.createQuery(jpql, Reservation.class)
                .setParameter("center", center)
                .setParameter("radius", radius)
                .setParameter("fromTime", fromTime)
                .setParameter("toTime", toTime)
                .getResultList();
    }

    public List<Reservation> findInArea(Point center, Double radius) {
        String jpql =
                "SELECT r FROM Reservation r " +
                "INNER JOIN ReservationAdSpace rs ON rs.reservation = r " +
                "INNER JOIN AdSpace s ON rs.adSpace = s " +
                "INNER JOIN AdLocation l ON s.adLocation = l " +
                "WHERE cast(st_dwithin(l.position, :center, :radius) AS boolean)";
        return entityManager.createQuery(jpql, Reservation.class)
                .setParameter("center", center)
                .setParameter("radius", radius)
                .getResultList();
    }

    public List<Reservation> findInTimeRange(Instant fromTime, Instant toTime) {
        String jpql =
                "SELECT r FROM Reservation r " +
                "INNER JOIN ReservationAdSpace rs ON rs.reservation = r " +
                "WHERE (rs.fromTime >= :fromTime AND rs.fromTime <= :toTime) " +
                "OR (rs.toTime >= :fromTime AND rs.toTime <= :toTime)";
        return entityManager.createQuery(jpql, Reservation.class)
                .setParameter("fromTime", fromTime)
                .setParameter("toTime", toTime)
                .getResultList();
    }

    public Optional<ReservationAdSpace> findReservationAdSpaceById(Integer id) {
        return Optional.ofNullable(
                entityManager.find(ReservationAdSpace.class, id));
    }

    @Transactional
    public Reservation saveWithConnections(Reservation reservation) {
        AdDesign blankAdDesign = generateBlankAdDesign(reservation);
        assignAdDesignToEachReservationAdSpaceWithNoAdDesign(reservation, blankAdDesign);
        entityManager.persist(reservation);
        return reservation;
    }

    @Transactional
    public Reservation updateAndOverwriteReservationAdSpaces(Reservation reservation, Integer id) {
        Reservation entityToSave = entityManager.find(Reservation.class, id);
        if (entityToSave != null) {
            AdDesign blankAdDesign = generateBlankAdDesign(reservation);
            assignAdDesignToEachReservationAdSpaceWithNoAdDesign(reservation, blankAdDesign);

            // remove existing ReservationAdSpaces that were already present
            reservation.getReservationAdSpaces()
                    .removeAll(entityToSave.getReservationAdSpaces());
            return entityManager.merge(reservation);
        }
        return null;
    }

    private void assignAdDesignToEachReservationAdSpaceWithNoAdDesign(Reservation reservation, AdDesign adDesign) {
        for (ReservationAdSpace rs : reservation.getReservationAdSpaces()) {
            if (rs.getAdDesign() == null) {
                rs.setAdDesign(adDesign);
            }
            rs.setReservation(reservation);
        }
    }

    private AdDesign generateBlankAdDesign(Reservation reservation) {
        AdDesign adDesign = new AdDesign();
        adDesign.setImageFileExists(false);
        adDesign.setReservation(reservation);
        reservation.getAdDesigns().add(adDesign);
        return adDesign;
    }
}
