package com.admanagement.app.persistence.repositories;

import com.admanagement.app.persistence.model.User;
import jakarta.transaction.Transactional;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public class UserRepository extends BaseRepository<User, Integer> {

    private final PasswordEncoder passwordEncoder;

    public UserRepository(PasswordEncoder passwordEncoder) {
        super(User.class);
        this.passwordEncoder = passwordEncoder;
    }

    public Optional<User> findByUsername(String username) {
        String jpql = "SELECT u FROM User u WHERE u.username = :username";
        List<User> resultList = entityManager.createQuery(jpql, User.class)
                .setParameter("username", username)
                .getResultList();
        return resultList.isEmpty() ? Optional.empty() : Optional.ofNullable(resultList.getFirst());
    }

    @Override
    public List<User> findAll() {
        String jpql = "SELECT u FROM User u WHERE u.enabled = true";
        return entityManager.createQuery(jpql, User.class).getResultList();
    }

    @Override
    @Transactional
    public User save(User entity) {
        if (entity.getEnabled() == null)
            entity.setEnabled(true);
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        entityManager.persist(entity);
        return entity;
    }

    @Override
    @Transactional
    public User update(User entity, Integer id) {
        User entityToSave = entityManager.find(User.class, id);
        if (entity.getEnabled() == null)
            entity.setEnabled(entityToSave.getEnabled());
        entity.setCreatedAt(entityToSave.getCreatedAt());
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        entityManager.merge(entity);
        return entity;
    }

    @Override
    public void deleteById(Integer id) {
        User entityToRemove = entityManager.find(User.class, id);
        if (entityToRemove != null) {
            entityToRemove.setEnabled(false);
            entityManager.persist(entityToRemove);
        }
    }
}
