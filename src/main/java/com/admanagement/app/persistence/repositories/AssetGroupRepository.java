package com.admanagement.app.persistence.repositories;

import com.admanagement.app.persistence.model.AssetGroup;
import org.springframework.stereotype.Repository;

@Repository
public class AssetGroupRepository extends BaseRepository<AssetGroup, Integer> {

    public AssetGroupRepository() {
        super(AssetGroup.class);
    }

}
