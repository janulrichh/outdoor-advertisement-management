package com.admanagement.app.persistence.repositories;

import com.admanagement.app.persistence.model.AdSpace;
import jakarta.transaction.Transactional;
import org.locationtech.jts.geom.Point;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;

@Repository
public class AdSpaceRepository extends BaseRepository<AdSpace, Integer> {

    public AdSpaceRepository() {
        super(AdSpace.class);
    }

    public List<AdSpace> findByAvailabilityInArea(Point center, Double radius,
                                                  Instant fromTime, Instant toTime) {
        String jpql =
                "SELECT s FROM AdSpace s " +
                "INNER JOIN AdLocation l ON s.adLocation = l " +
                "LEFT JOIN ReservationAdSpace rs ON rs.adSpace = s " +
                "WHERE s.enabled " +
                "AND (rs.adSpace IS NULL OR rs.toTime < :fromTime OR rs.fromTime > :toTime) " +
                "AND cast(st_dwithin(l.position, :center, :radius) AS boolean) ";
        return entityManager.createQuery(jpql, AdSpace.class)
                .setParameter("center", center)
                .setParameter("radius", radius)
                .setParameter("fromTime", fromTime)
                .setParameter("toTime", toTime)
                .getResultList();
    }

    public List<AdSpace> findInArea(Point center, Double radius) {
        String query =
                "SELECT s FROM AdSpace s " +
                "INNER JOIN AdLocation l ON s.adLocation = l " +
                "WHERE s.enabled " +
                "AND cast(st_dwithin(l.position, :center, :radius) AS boolean)";
        return entityManager.createQuery(query, AdSpace.class)
                .setParameter("center", center)
                .setParameter("radius", radius)
                .getResultList();
    }

    public List<AdSpace> findByAvailability(Instant fromTime, Instant toTime) {
        String query =
                "SELECT s FROM AdSpace s " +
                "LEFT JOIN ReservationAdSpace rs " +
                "ON rs.adSpace = s " +
                "WHERE s.enabled " +
                "AND rs.adSpace IS NULL " +
                "OR (rs.toTime < :fromTime OR rs.fromTime > :toTime)";
        return entityManager.createQuery(query, AdSpace.class)
                .setParameter("fromTime", fromTime)
                .setParameter("toTime", toTime)
                .getResultList();
    }

    @Override
    public List<AdSpace> findAll() {
        String jpql = "SELECT s FROM AdSpace s WHERE s.enabled";
        return entityManager.createQuery(jpql, AdSpace.class).getResultList();
    }

    @Override
    @Transactional
    public AdSpace save(AdSpace entity) {
        if (entity.getEnabled() == null)
            entity.setEnabled(true);
        entityManager.persist(entity);
        return entity;
    }

    @Override
    @Transactional
    public AdSpace update(AdSpace entity, Integer id) {
        AdSpace entityToSave = entityManager.find(AdSpace.class, id);
        if (entity.getEnabled() == null)
            entity.setEnabled(entityToSave.getEnabled());
        entityManager.merge(entity);
        return entity;
    }

    @Override
    public void deleteById(Integer id) {
        AdSpace entityToRemove = entityManager.find(AdSpace.class, id);
        if (entityToRemove != null) {
            entityToRemove.setEnabled(false);
            entityManager.persist(entityToRemove);
        }
    }
}
