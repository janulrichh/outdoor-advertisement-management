package com.admanagement.app.persistence.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SourceType;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class Reservation extends BaseEntity {

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "clientId", nullable = false)
    private Client client;

    @ManyToOne
    @JoinColumn(name = "creatorUserId", nullable = false)
    private User creatorUser;

    @NotNull
    @Size(max = 100)
    private String campaignName;

    @NotNull
    private Boolean cancelled;

    @CreationTimestamp(source = SourceType.DB)
    @Column(name = "createdAt", nullable = false, updatable = false, insertable = false)
    private Instant createdAt;

    private Instant approvedAt;

    @Size(max = 30)
    private String invoiceNumber;

    @Size(max = 3000)
    private String comment;

    @OneToMany(mappedBy = "reservation", cascade = CascadeType.ALL)
    private List<AdDesign> adDesigns = new ArrayList<>();

    @OneToMany(mappedBy = "reservation", cascade = CascadeType.ALL)
    private List<ReservationAdSpace> reservationAdSpaces = new ArrayList<>();

}
