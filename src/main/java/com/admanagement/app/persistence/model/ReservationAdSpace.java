package com.admanagement.app.persistence.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
@Entity
public class ReservationAdSpace extends BaseEntity {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "adSpaceId", nullable = false)
    private AdSpace adSpace;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "reservationId", nullable = false)
    private Reservation reservation;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "adDesignId", nullable = false)
    private AdDesign adDesign;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "installerUserId", nullable = false)
    private User installerUser;

    @NotNull
    private Instant fromTime;

    @NotNull
    private Instant toTime;

    private Instant adInstalledAt;
}
