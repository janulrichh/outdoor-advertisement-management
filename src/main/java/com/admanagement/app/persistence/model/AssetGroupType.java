package com.admanagement.app.persistence.model;

import lombok.Getter;

@Getter
public enum AssetGroupType {
    AD_LOCATION,
    AD_SPACE,
    COMBINED
}