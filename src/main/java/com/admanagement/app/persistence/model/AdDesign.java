package com.admanagement.app.persistence.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
public class AdDesign extends BaseEntity {

    @NotNull
    private Boolean imageFileExists;

    private byte[] image;

    @Size(max = 255)
    private String imageFileExtension;

    @Size(max = 260)
    private String imageFileName;

    private Instant imageUploadTime;

    @Size(max = 24)
    private String publicId;

    @OneToMany(mappedBy = "adDesign", cascade = CascadeType.REMOVE)
    private List<ReservationAdSpace> reservationAdSpaces = new ArrayList<>();

    @OneToMany(mappedBy = "adDesign", cascade = CascadeType.REMOVE)
    private List<AdDesignOrder> adDesignOrders = new ArrayList<>();

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "reservationId", nullable = false)
    private Reservation reservation;

    public String getFileNameWithExtension() {
        return imageFileName + "." + imageFileExtension;
    }
}


