package com.admanagement.app.persistence.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class AdSpace extends BaseEntity {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "adLocationId", nullable = false)
    private AdLocation adLocation;

    @Size(max = 30)
    @NotNull
    private String locator;

    @NotNull
    private Boolean enabled;

    @Size(max = 3000)
    private String comment;

    @ManyToMany
    @JoinTable(name = "assetGroupAdSpace",
            joinColumns = @JoinColumn(name = "adSpaceId"),
            inverseJoinColumns = @JoinColumn(name = "assetGroupId"))
    private List<AssetGroup> assetGroups = new ArrayList<>();

    @OneToMany(mappedBy = "adSpace")
    private List<ReservationAdSpace> reservationAdSpaces = new ArrayList<>();

    private byte[] image;

    @Size(max = 255)
    private String imageFileExtension;

    @Size(max = 260)
    private String imageFileName;

    private Instant imageUploadTime;

    public String getImageFileNameWithExtension() {
        return imageFileName + "." + imageFileExtension;
    }

}