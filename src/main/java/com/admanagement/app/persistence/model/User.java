package com.admanagement.app.persistence.model;

import com.admanagement.app.common.security.Role;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SourceType;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@Entity
@Table(name = "UserAccount")
public class User extends BaseEntity {

    @NotNull
    @Size(max = 255)
    private String username;

    @NotNull
    @Size(max = 255)
    private String password;

    @NotNull
    private Boolean enabled;

    @NotNull
    @Size(max = 50)
    private String firstName;

    @NotNull
    @Size(max = 50)
    private String lastName;

    @CreationTimestamp(source = SourceType.DB)
    @Column(name = "createdAt", nullable = false, updatable = false, insertable = false)
    private Instant createdAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userRoleId", referencedColumnName = "id", nullable = false)
    private UserRole userRole;

    @OneToMany(mappedBy = "creatorUser")
    private List<Client> clients = new ArrayList<>();

    @OneToMany(mappedBy = "creatorUser")
    private List<Reservation> reservations = new ArrayList<>();

    @OneToMany(mappedBy = "installerUser")
    private List<ReservationAdSpace> reservationAdSpaces = new ArrayList<>();

    @OneToMany(mappedBy = "orderUser")
    private List<AdDesignOrder> adDesignOrders = new ArrayList<>();

    public Boolean hasAdminRole() {
        return userRole.getAuthority().equals(Role.ROLE_ADMIN.name());
    }
}
