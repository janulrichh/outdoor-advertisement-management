package com.admanagement.app.persistence.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.locationtech.jts.geom.Point;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@EqualsAndHashCode(callSuper = false)
public class AdLocation extends BaseEntity {

    @NotNull
    @Size(max = 10)
    private String number;

    @NotNull
    private Boolean enabled;

    @Size(max = 50)
    private String stopName;

    @Size(max = 50)
    private String streetName;

    private Character direction;

    @Size(max = 3000)
    private String comment;

    @OneToMany(mappedBy = "adLocation", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<AdSpace> adSpaces = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "assetGroupAdLocation",
            joinColumns = @JoinColumn(name = "adLocationId"),
            inverseJoinColumns = @JoinColumn(name = "assetGroupId"))
    private List<AssetGroup> assetGroups = new ArrayList<>();

    @Column(columnDefinition = "GEOGRAPHY(Point,4326)")
    private Point position;

}

