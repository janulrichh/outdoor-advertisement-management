package com.admanagement.app.persistence.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class AssetGroup extends BaseEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "assetGroupType", nullable = false)
    private AssetGroupType assetGroupType;

    @NotNull
    @Size(max = 50)
    private String name;

    @Size(max = 255)
    private String description;

    @ManyToMany(mappedBy = "assetGroups", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<AdLocation> adLocations = new ArrayList<>();

    @ManyToMany(mappedBy = "assetGroups", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<AdSpace> adSpaces = new ArrayList<>();
}
