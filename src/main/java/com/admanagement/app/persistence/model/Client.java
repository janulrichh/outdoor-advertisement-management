package com.admanagement.app.persistence.model;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SourceType;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class Client extends BaseEntity {

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "creatorUserId", nullable = false)
    private User creatorUser;

    @Size(max = 128)
    private String name;

    @Size(max = 128)
    private String contactPersonName;

    @Size(max = 20)
    private String contactPhone;

    @Size(max = 255)
    private String email;

    @NotNull
    private Boolean enabled;

    @CreationTimestamp(source = SourceType.DB)
    @Column(name = "createdAt", nullable = false, updatable = false, insertable = false)
    private Instant createdAt;

    @Size(max = 3000)
    private String comment;

    @OneToMany(mappedBy = "id")
    private List<Reservation> reservations = new ArrayList<>();

}