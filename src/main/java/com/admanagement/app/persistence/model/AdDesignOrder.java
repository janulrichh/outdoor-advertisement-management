package com.admanagement.app.persistence.model;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.SourceType;

import java.time.Instant;

@Getter
@Setter
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class AdDesignOrder extends BaseEntity {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "orderUserId", referencedColumnName = "id", nullable = false)
    private User orderUser;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "adDesignId", referencedColumnName = "id", nullable = false)
    private AdDesign adDesign;

    @CreationTimestamp(source = SourceType.DB)
    @Column(name = "orderedAt", nullable = false, updatable = false, insertable = false)
    private Instant orderedAt;

    @NotNull
    private Integer orderAmount;

}
