package com.admanagement.app.common;

import jakarta.persistence.EntityManager;
import jakarta.persistence.Query;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationReader {

    private final EntityManager entityManager;

    public ConfigurationReader(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    public String getConfigurationValue(String configurationKey) {
        Query query = entityManager.createNativeQuery(
                "SELECT (value) FROM configuration WHERE configuration.key = :key")
                .setParameter("key", configurationKey);
        return query.getSingleResult().toString();
    }

    @Transactional
    public void setConfigurationValue(String configurationKey, String configurationValue) {
        Query query = entityManager.createNativeQuery("UPDATE configuration SET value = :value WHERE key = :key")
                .setParameter("value", configurationValue)
                .setParameter("key", configurationKey);
        query.executeUpdate();
    }
}
