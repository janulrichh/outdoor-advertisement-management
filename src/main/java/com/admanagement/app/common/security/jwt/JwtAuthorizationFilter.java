package com.admanagement.app.common.security.jwt;

import com.admanagement.app.common.security.TokenInfo;
import io.jsonwebtoken.ExpiredJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.lang.NonNull;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

public class JwtAuthorizationFilter extends OncePerRequestFilter {

    private final String jwtKey;

    public JwtAuthorizationFilter(String jwtKey) {
        this.jwtKey = jwtKey;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    @NonNull HttpServletResponse response,
                                    @NonNull FilterChain chain) throws IOException, ServletException {

        String tokenString = request.getHeader("Authorization");

        if (tokenString == null) {
            chain.doFilter(request, response);
            return;
        }

        TokenInfo tokenInfo;
        try {
            tokenInfo = new JwtHelper(jwtKey).decode(tokenString);
        } catch (ExpiredJwtException e) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            chain.doFilter(request, response);
            return;
        }

        var authorities = tokenInfo.getRoles().stream()
                .map(SimpleGrantedAuthority::new)
                .toList();

        var springToken = new UsernamePasswordAuthenticationToken(tokenInfo.getUsername(), null, authorities);

        SecurityContextHolder.getContext().setAuthentication(springToken);

        chain.doFilter(request, response);
    }
}