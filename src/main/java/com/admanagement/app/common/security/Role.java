package com.admanagement.app.common.security;

import lombok.Getter;
import org.springframework.security.core.Authentication;

@Getter
public enum Role {

    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_SALES("ROLE_SALES"),
    ROLE_INSTALL("ROLE_INSTALL");

    private final String role;

    Role(String authority) {
        this.role = authority;
    }

    public static boolean hasAuthority(Authentication authentication, Role role) {
        return authentication.getAuthorities().stream()
                .anyMatch(grantedAuthority -> grantedAuthority.getAuthority().equals(role.getRole()));
    }
}
