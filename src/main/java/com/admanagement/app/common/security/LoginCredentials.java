package com.admanagement.app.common.security;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Authentication request data")
public class LoginCredentials {

    @Schema(description = "Username of the user account.", example = "admin")
    private String username;

    @Schema(description = "Password of the user account as plain text.", example = "secret")
    private String password;

}
