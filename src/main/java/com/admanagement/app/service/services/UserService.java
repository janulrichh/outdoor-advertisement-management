package com.admanagement.app.service.services;

import com.admanagement.app.persistence.model.User;
import com.admanagement.app.persistence.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService extends BaseService<User, Integer, UserRepository> {

    public UserService(UserRepository repository) {
        super(repository);
    }

    public Optional<User> findByUsername(String username) {
        return repository.findByUsername(username);
    }

    public Boolean canBeDeleted(Integer id) {
        Optional<User> userToDelete = repository.findById(id);
        if (userToDelete.isEmpty()) return null;
        if (!userToDelete.get().hasAdminRole()) return true;
        return repository.findAll().stream().filter(User::hasAdminRole).toList().size() > 1;  // one admin must remain
    }
}
