package com.admanagement.app.service.services;

import com.admanagement.app.persistence.model.AdDesign;
import com.admanagement.app.persistence.model.Reservation;
import com.admanagement.app.persistence.model.ReservationAdSpace;
import com.admanagement.app.persistence.model.User;
import com.admanagement.app.persistence.repositories.AdDesignRepository;
import com.admanagement.app.persistence.repositories.ReservationRepository;
import com.admanagement.app.service.utils.FileUtility;
import com.admanagement.app.service.utils.GeometryUtility;
import com.admanagement.app.service.utils.ImageUtility;
import org.locationtech.jts.geom.Point;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationService extends BaseService<Reservation, Integer, ReservationRepository> {

    private final AdDesignRepository adDesignRepository;
    private final Environment env;

    public ReservationService(ReservationRepository repository, AdDesignRepository adDesignRepository, Environment env) {
        super(repository);
        this.adDesignRepository = adDesignRepository;
        this.env = env;
    }

    public List<Reservation> findMatching(Double latitude, Double longitude, Double radius,
                                          String fromTime, String toTime) {

        Instant fromTimeInstant = fromTime != null ? Instant.parse(fromTime) : null;
        Instant toTimeInstant = toTime != null ? Instant.parse(toTime) : null;

        List<Reservation> results = new ArrayList<>();

        if (latitude != null && longitude != null) {
            Point center = GeometryUtility.toPoint(latitude, longitude);
            if (fromTime != null && toTime != null) {
                results.addAll(repository.findInTimeRangeInArea(center, radius, fromTimeInstant, toTimeInstant));
            } else {
                results.addAll(repository.findInArea(center, radius));
            }
        } else if (fromTime != null && toTime != null) {
            results.addAll(repository.findInTimeRange(fromTimeInstant, toTimeInstant));
        } else {
            results.addAll(repository.findAll());
        }
        return results;
    }

    public List<Reservation> findReservationsThatRequireAdDesignInstallation() {
        return findReservationsThatRequireAdDesignInstallation(null, null, null);
    }

    public List<Reservation> findReservationsThatRequireAdDesignInstallation(Double latitude, Double longitude, Double radius) {
        if (latitude == null || longitude == null || radius == null) {
            return repository.findAllToInstall();
        }
        Point center = GeometryUtility.toPoint(latitude, longitude);
        return repository.findInAreaToInstall(center, radius);
    }

    public Optional<ReservationAdSpace> findReservationAdSpaceById(Integer id) {
        return repository.findReservationAdSpaceById(id);
    }

    public Optional<AdDesign> findAdDesignById(Integer reservationId, Integer imageId) {
        Optional<AdDesign> adDesign = adDesignRepository.findById(imageId);
        if (adDesign.isEmpty() || !adDesign.get().getReservation().getId().equals(reservationId)) {
            return Optional.empty();
        }
        return adDesign;
    }

    public byte[] getReservationAdDesignResizedImage(AdDesign adDesign, Integer maxWidth, Integer maxHeight) throws IOException {
        if (adDesign == null)
            throw new RuntimeException("AdDesign not found");
        if (maxWidth == null || maxHeight == null) {
            return adDesign.getImage();
        }
        return ImageUtility.resizeImage(adDesign.getImage(), adDesign.getImageFileExtension(), maxWidth, maxHeight);
    }

    @Override
    public Reservation save(Reservation entity) {
        return repository.saveWithConnections(entity);
    }

    @Override
    public Reservation update(Reservation entity, Integer id) {
        return repository.updateAndOverwriteReservationAdSpaces(entity, id);
    }

    public void addAdDesignImageToReservation(Integer reservationId, MultipartFile file) throws IOException {

        Optional<Reservation> reservation = repository.findById(reservationId);
        if (reservation.isEmpty()) {
            throw new RuntimeException("No corresponding Reservation to add AdDesign");
        }

        try {
            byte[] imageBytes = file.getBytes();
            String imageFileName = file.getOriginalFilename();
            if (imageFileName == null) {
                throw new IOException("Failed to process the image.");
            }
            AdDesign adDesign = new AdDesign();
            adDesign.setImage(imageBytes);
            adDesign.setImageFileExists(true);
            adDesign.setImageFileName(FileUtility.getFileNameWithoutExtension(imageFileName));
            adDesign.setImageFileExtension(FileUtility.getFileExtension(imageFileName));
            adDesign.setImageUploadTime(Instant.now());
            adDesign.setReservation(reservation.get());
            reservation.get().getAdDesigns().add(adDesign);
            adDesignRepository.save(adDesign);
        } catch (IOException e) {
            throw new IOException("Failed to process the image.");
        }
    }

    public void updateInstallationStatusOfReservationAdSpace(Integer id, User installerUser) {
        Optional<ReservationAdSpace> reservationAdSpace = repository.findReservationAdSpaceById(id);
        if (reservationAdSpace.isEmpty()) return;
        reservationAdSpace.get().setAdInstalledAt(Instant.now());
        reservationAdSpace.get().setInstallerUser(installerUser);
        repository.save(reservationAdSpace.get().getReservation());
    }

    public void reverseInstallationStatusOfReservationAdSpace(Integer id) {
        Optional<ReservationAdSpace> reservationAdSpace = repository.findReservationAdSpaceById(id);
        if (reservationAdSpace.isEmpty()) return;
        reservationAdSpace.get().setAdInstalledAt(null);
        repository.save(reservationAdSpace.get().getReservation());
    }

    @Scheduled(cron = "0 0 0 1 * *") // Run at midnight on the 1st day of every month
    public void deleteOldAdDesignImages() {
        if (Boolean.getBoolean(env.getProperty("reservation.service.delete.old.designs"))) {
            adDesignRepository.deleteOldAdDesignImages();
        }
    }
}
