package com.admanagement.app.service.services;

import com.admanagement.app.persistence.model.Client;
import com.admanagement.app.persistence.repositories.ClientRepository;
import org.springframework.stereotype.Service;

@Service
public class ClientService extends BaseService<Client, Integer, ClientRepository> {

    public ClientService(ClientRepository repository) {
        super(repository);
    }

}
