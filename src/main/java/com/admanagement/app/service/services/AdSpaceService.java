package com.admanagement.app.service.services;

import com.admanagement.app.persistence.model.AdSpace;
import com.admanagement.app.persistence.repositories.AdSpaceRepository;
import com.admanagement.app.service.utils.FileUtility;
import com.admanagement.app.service.utils.GeometryUtility;
import com.admanagement.app.service.utils.ImageUtility;
import org.locationtech.jts.geom.Point;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AdSpaceService extends BaseService<AdSpace, Integer, AdSpaceRepository> {

    public AdSpaceService(AdSpaceRepository repository) {
        super(repository);
    }

    public List<AdSpace> findMatching(Double latitude, Double longitude, Double radius,
                                      String fromTime, String toTime) {

        Instant fromTimeInstant = fromTime != null ? Instant.parse(fromTime) : null;
        Instant toTimeInstant = toTime != null ? Instant.parse(toTime) : null;

        List<AdSpace> results = new ArrayList<>();

        if (latitude != null && longitude != null) {
            Point center = GeometryUtility.toPoint(latitude, longitude);
            if (fromTime != null && toTime != null) {
                results.addAll(repository.findByAvailabilityInArea(center, radius, fromTimeInstant, toTimeInstant));
            } else {
                results.addAll(repository.findInArea(center, radius));
            }
        } else if (fromTime != null && toTime != null) {
            results.addAll(repository.findByAvailability(fromTimeInstant, toTimeInstant));
        } else {
            results.addAll(repository.findAll());
        }
        return results;
    }

    public void updateImage(Integer id, MultipartFile file) throws IOException {
        try {
            byte[] imageBytes = file.getBytes();
            repository.findById(id).ifPresent(adSpace -> {
                adSpace.setImage(imageBytes);
                adSpace.setImageUploadTime(Instant.now());
                if (file.getOriginalFilename() != null) {
                    adSpace.setImageFileName(FileUtility.getFileNameWithoutExtension(file.getOriginalFilename()));
                    adSpace.setImageFileExtension(FileUtility.getFileExtension(file.getOriginalFilename()));
                }
                repository.update(adSpace, adSpace.getId());
            });
        } catch (IOException e) {
            throw new IOException("Failed to process the image.");
        }
    }

    public byte[] getResizedImage(Integer id, Integer maxWidth, Integer maxHeight) throws IOException {
        Optional<AdSpace> adSpace = repository.findById(id);
        if (adSpace.isEmpty()) {
            throw new RuntimeException("Image of AdSpace not found");
        }
        if (maxWidth == null || maxHeight == null) {
            return adSpace.get().getImage();
        }
        return ImageUtility.resizeImage(adSpace.get().getImage(), adSpace.get().getImageFileExtension(), maxWidth, maxHeight);
    }
}
