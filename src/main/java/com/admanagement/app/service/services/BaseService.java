package com.admanagement.app.service.services;

import com.admanagement.app.persistence.repositories.AbstractRepository;

import java.util.List;
import java.util.Optional;

public abstract class BaseService<T, ID, R extends AbstractRepository<T, ID>> {

    public BaseService(R repository) {
        this.repository = repository;
    }

    protected final R repository;

    public T save(T entity) {
        return repository.save(entity);
    }

    public T update(T entity, ID id) {
        return repository.update(entity, id);
    }

    public Optional<T> findById(ID id) {
        return repository.findById(id);
    }

    public List<T> findAll() {
        return repository.findAll();
    }

    public void deleteById(ID id) {
        repository.deleteById(id);
    }
}


