package com.admanagement.app.service.services;

import com.admanagement.app.persistence.model.AdLocation;
import com.admanagement.app.persistence.repositories.AdLocationRepository;
import com.admanagement.app.service.utils.GeometryUtility;
import org.locationtech.jts.geom.Point;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdLocationService extends BaseService<AdLocation, Integer, AdLocationRepository> {

    public AdLocationService(AdLocationRepository repository) {
        super(repository);
    }

    public List<AdLocation> findMatching(Double latitude, Double longitude, Double radius) {
        if (longitude != null && latitude != null && radius != null) {
            Point point = GeometryUtility.toPoint(latitude, longitude);
            return repository.findInArea(point, radius);
        } else {
            return repository.findAll();
        }
    }
}
