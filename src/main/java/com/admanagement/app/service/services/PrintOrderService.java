package com.admanagement.app.service.services;

import com.admanagement.app.common.ConfigurationReader;
import com.admanagement.app.persistence.model.AdDesign;
import com.admanagement.app.persistence.model.AdDesignOrder;
import com.admanagement.app.persistence.model.ReservationAdSpace;
import com.admanagement.app.persistence.model.User;
import com.admanagement.app.persistence.repositories.AdDesignOrderRepository;
import com.admanagement.app.persistence.repositories.AdDesignRepository;
import com.admanagement.app.presentation.dtos.PublicPrintServiceAddress;
import jakarta.mail.MessagingException;
import jakarta.mail.internet.MimeMessage;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

@Service
public class PrintOrderService {

    private final AdDesignRepository adDesignRepository;
    private final AdDesignOrderRepository adDesignOrderRepository;
    private final ConfigurationReader configurationReader;
    private final JavaMailSender mailSender;
    private final Environment env;

    public PrintOrderService(AdDesignRepository adDesignRepository, AdDesignOrderRepository adDesignOrderRepository, ConfigurationReader configurationReader, JavaMailSender mailSender, Environment env) {
        this.adDesignRepository = adDesignRepository;
        this.adDesignOrderRepository = adDesignOrderRepository;
        this.configurationReader = configurationReader;
        this.mailSender = mailSender;
        this.env = env;
    }

    public Optional<AdDesign> findAdDesignByPublicId(String publicId) {
        return adDesignRepository.findByPublicId(publicId);
    }

    public List<AdDesign> findAllWaitingToOrder() {
        return adDesignRepository.findAllWaitingToOrder();
    }

    public void updateAddress(PublicPrintServiceAddress address) {
        configurationReader.setConfigurationValue("ORDER_RECIPIENT", address.getEmail());
    }

    public void orderAdDesigns(Map<Integer, Integer> adDesignAmounts, User orderUser, String requestUrl) throws MessagingException {
        // create AdDesignOrders
        List<AdDesignOrder> adDesignOrders = createAdDesignOrders(adDesignAmounts, orderUser);

        // send email
        composeAndSendOrdersMail(adDesignOrders, requestUrl);

        // save successful AdDesignOrders
        adDesignOrders.forEach(adDesignOrderRepository::save);
    }

    private List<AdDesignOrder> createAdDesignOrders(Map<Integer, Integer> adDesignAmounts, User orderUser) {
        List<AdDesignOrder> adDesignOrders = new ArrayList<>();
        for (var entry : adDesignAmounts.entrySet()) {
            Integer adDesignId = entry.getKey();
            Integer orderAmount = entry.getValue();

            Optional<AdDesign> adDesignBeingOrdered = adDesignRepository.findById(adDesignId);
            if (adDesignBeingOrdered.isEmpty()) {
                throw new RuntimeException("AdDesign not found.");
            }
            if (adDesignBeingOrdered.get().getImageFileExists()) {
                adDesignBeingOrdered.get().setPublicId(generateRandomToken());
                adDesignOrders.add(getAdDesignOrder(adDesignBeingOrdered.get(), orderAmount, orderUser));
            }
        }
        return adDesignOrders;
    }

    private AdDesignOrder getAdDesignOrder(AdDesign adDesign, Integer amount, User orderUser) {
        AdDesignOrder adDesignOrder = new AdDesignOrder();
        adDesignOrder.setAdDesign(adDesign);
        adDesignOrder.setOrderAmount(amount);
        adDesignOrder.setOrderUser(orderUser);
        return adDesignOrder;
    }

    private void composeAndSendOrdersMail(List<AdDesignOrder> adDesignOrders, String requestUrl) throws MessagingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, "utf-8");

        String from = env.getProperty("spring.mail.username");
        String to = configurationReader.getConfigurationValue("ORDER_RECIPIENT");
        String subject = env.getProperty("print.service.mail.subject");

        Integer orderTimeBuffer = Integer.parseInt(Objects.requireNonNull(env.getProperty("print.service.mail.order.time.buffer.days")));

        if (from == null || to == null || subject == null)
            throw new RuntimeException("Check environment variables for errors");

        String bodyText = env.getProperty("print.service.mail.body");

        helper.setFrom(from);
        helper.setTo(to);
        helper.setSubject(subject);

        helper.setText(this.buildMailBody(adDesignOrders, requestUrl, bodyText, orderTimeBuffer), true);
        mailSender.send(mimeMessage);
    }

    private String buildMailBody(List<AdDesignOrder> adDesignOrders, String requestUrl, String bodyText, Integer orderTimeBuffer) {
        StringBuilder body = new StringBuilder("<html><body>" + bodyText);

        for (AdDesignOrder adDesignOrder : adDesignOrders) {
            AdDesign adDesign = adDesignOrder.getAdDesign();

            String adDesignImagePublicUrl = String.format("%s/api/print/order/%s", requestUrl, adDesign.getPublicId());
            String adDesignImageHref = String.format("<a href=\"%s\">%s</a>", adDesignImagePublicUrl, adDesign.getImageFileName());

            Optional<ReservationAdSpace> earliestUseOfReservationAdSpace = adDesignOrder.getAdDesign()
                    .getReservationAdSpaces().stream()
                    .min(Comparator.comparing(ReservationAdSpace::getFromTime));
            if (earliestUseOfReservationAdSpace.isEmpty())
                throw new RuntimeException("Order should not contain AdDesigns that are not being used.");

            Instant deadline = earliestUseOfReservationAdSpace.get().getFromTime().minus(orderTimeBuffer, ChronoUnit.DAYS);
            Integer amount = adDesignOrder.getOrderAmount();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy").withZone(ZoneId.systemDefault());

            String orderLineHtml = String.format(
                    "<p> --> quantity: %s | deadline: %s | file: %s</p>", amount, formatter.format(deadline), adDesignImageHref);

            body.append("<p>").append(orderLineHtml).append("</p>");
        }

        body.append("</body></html>");
        return body.toString();
    }

    private String generateRandomToken() {
        byte[] bytes = new byte[16];
        new SecureRandom().nextBytes(bytes);
        return Base64.getUrlEncoder().withoutPadding().encodeToString(bytes);
    }
}
