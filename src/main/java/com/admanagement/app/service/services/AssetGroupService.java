package com.admanagement.app.service.services;

import com.admanagement.app.persistence.model.AssetGroup;
import com.admanagement.app.persistence.repositories.AssetGroupRepository;
import org.springframework.stereotype.Service;

@Service
public class AssetGroupService extends BaseService<AssetGroup, Integer, AssetGroupRepository> {

    public AssetGroupService(AssetGroupRepository repository) {
        super(repository);
    }

}
