package com.admanagement.app.service.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


public class ImageUtility {

    public static byte[] resizeImage(byte[] imageBytes, String fileFormat, int maxWidth, int maxHeight) throws IOException {
        // Read the input image byte array
        ByteArrayInputStream inputStream = new ByteArrayInputStream(imageBytes);
        BufferedImage inputImage = ImageIO.read(inputStream);

        // Find new dimensions limited by maxWidth, maxHeight
        int[] newDimensions = calculateNewDimensionsWhilePreservingAspectRatio(
                inputImage.getWidth(), inputImage.getHeight(), maxWidth, maxHeight);
        int width = newDimensions[0];
        int height = newDimensions[1];

        // Create a new resized image with the calculated dimensions
        BufferedImage resizedImage = new BufferedImage(width, height, inputImage.getType());
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(inputImage, 0, 0, width, height, null);
        g.dispose();

        // Write the resized image to a byte array output stream
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ImageIO.write(resizedImage, fileFormat, outputStream);
        return outputStream.toByteArray();
    }

    private static int[] calculateNewDimensionsWhilePreservingAspectRatio(int originalWidth, int originalHeight,
                                                                          int maxWidth, int maxHeight) {
        // Calculate the aspect ratio
        double aspectRatio = 1.0 * originalWidth / originalHeight;

        // Calculate new dimensions to fit within the specified maxWidth and maxHeight while preserving aspect ratio
        int newWidth = originalWidth;
        int newHeight = originalHeight;
        if (originalWidth > maxWidth) {
            newWidth = maxWidth;
            newHeight = Math.toIntExact(Math.round(newWidth / aspectRatio));
        }
        if (newHeight > maxHeight) {
            newHeight = maxHeight;
            newWidth = Math.toIntExact(Math.round(newHeight * aspectRatio));
        }

        return new int[] {newWidth, newHeight};
    }
}
