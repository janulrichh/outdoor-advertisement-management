package com.admanagement.app.service.utils;

public class FileUtility {

    public static String getFileExtension(String filenameWithExtension) {
        int dotIndex = filenameWithExtension.lastIndexOf('.');
        if (dotIndex != -1 && dotIndex < filenameWithExtension.length() - 1) {
            return filenameWithExtension.substring(dotIndex + 1);
        }
        return "";
    }

    public static String getFileNameWithoutExtension(String filenameWithExtension) {
        int dotIndex = filenameWithExtension.lastIndexOf('.');
        if (dotIndex != -1) {
            return filenameWithExtension.substring(0, dotIndex);
        }
        return filenameWithExtension;
    }
}
