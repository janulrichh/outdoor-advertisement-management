package com.admanagement.app.presentation.mappers;

import com.admanagement.app.persistence.model.*;
import com.admanagement.app.presentation.dtos.PublicAssetGroup;
import jakarta.persistence.EntityManager;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AssetGroupMapper {

    // PUBLIC DTO: PublicAssetGroup
    // DOMAIN CLASS: AssetGroup

    @Mapping(target = "adLocations", ignore = true)  // @AfterMapping
    @Mapping(target = "adSpaces", ignore = true)    // @AfterMapping
    AssetGroup publicToDomain(PublicAssetGroup source, @Context EntityManager entityManager);

    @Mapping(target = "adLocationIds", ignore = true)   // @BeforeMapping
    @Mapping(target = "adSpaceIds", ignore = true)      // @BeforeMapping
    PublicAssetGroup domainToPublic(AssetGroup source);

    @Mapping(target = "adLocationIds", ignore = true)   // @BeforeMapping
    @Mapping(target = "adSpaceIds", ignore = true)      // @BeforeMapping
    List<PublicAssetGroup> domainToPublicList(List<AssetGroup> sourceList);

    @AfterMapping
    default void afterMappingToDomain(PublicAssetGroup source, @MappingTarget AssetGroup target,
                                       @Context EntityManager entityManager) {
        List<AdLocation> adLocations = source.getAdLocationIds().stream()
                .map(id -> entityManager.find(AdLocation.class, id)).toList();
        target.setAdLocations(adLocations);

        List<AdSpace> adSpaces = source.getAdSpaceIds().stream()
                .map(id -> entityManager.find(AdSpace.class, id)).toList();
        target.setAdSpaces(adSpaces);
    }
    
    @BeforeMapping
    default void beforeMappingToPublic(AssetGroup source, @MappingTarget PublicAssetGroup target) {
        target.setAdLocationIds(source.getAdLocations().stream().map(BaseEntity::getId).toList());
        target.setAdSpaceIds(source.getAdSpaces().stream().map(BaseEntity::getId).toList());
    }
}
