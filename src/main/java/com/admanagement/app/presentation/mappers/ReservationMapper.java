package com.admanagement.app.presentation.mappers;

import com.admanagement.app.persistence.model.*;
import com.admanagement.app.presentation.dtos.PublicReservation;
import jakarta.persistence.EntityManager;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = ReservationAdSpaceMapper.class)
public interface ReservationMapper {

    // PUBLIC DTO: PublicReservation
    // DOMAIN CLASS: Reservation

    @Mapping(target = "adDesigns", ignore = true)
    @Mapping(target = "client", ignore = true)
    @Mapping(target = "creatorUser", ignore = true)
    Reservation publicToDomain(PublicReservation target, @Context EntityManager entityManager);

    @Mapping(target = "clientId", source = "client.id")
    @Mapping(target = "clientName", source = "client.name")
    @Mapping(target = "creatorUserId", source = "creatorUser.id")
    @Mapping(target = "creatorUserFirstName", source = "creatorUser.firstName")
    @Mapping(target = "creatorUserLastName", source = "creatorUser.lastName")
    @Mapping(target = "adDesignIds", ignore = true)
    PublicReservation domainToPublic(Reservation target);

    @Mapping(target = "clientId", source = "client.id")
    @Mapping(target = "clientName", source = "client.name")
    @Mapping(target = "creatorUserId", source = "creatorUser.id")
    @Mapping(target = "creatorUserFirstName", source = "creatorUser.firstName")
    @Mapping(target = "creatorUserLastName", source = "creatorUser.lastName")
    @Mapping(target = "adDesignIds", ignore = true)
    List<PublicReservation> domainToPublicList(List<Reservation> sourceList);

    @BeforeMapping
    default void beforeMappingToPublic(Reservation source, @MappingTarget PublicReservation target) {
        List<Integer> adDesignIds = source.getAdDesigns().stream().map(BaseEntity::getId).toList();
        target.setAdDesignIds(adDesignIds);
    }

    @AfterMapping
    default void afterMappingToDomain(PublicReservation source, @MappingTarget Reservation target,
                                      @Context EntityManager entityManager) {
        Client client = entityManager.find(Client.class, source.getClientId());
        User user = entityManager.find(User.class, source.getCreatorUserId());

        target.setClient(client);
        target.setCreatorUser(user);
    }
}
