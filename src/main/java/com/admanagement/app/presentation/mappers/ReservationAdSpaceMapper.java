package com.admanagement.app.presentation.mappers;

import com.admanagement.app.persistence.model.*;
import com.admanagement.app.presentation.dtos.PublicReservationAdSpace;
import jakarta.persistence.EntityManager;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ReservationAdSpaceMapper {

    // PUBLIC DTO: PublicReservationAdSpace
    // DOMAIN CLASS: ReservationAdSpace

    @Mapping(target = "adSpace", ignore = true)     // @AfterMapping
    @Mapping(target = "reservation", ignore = true)     // @AfterMapping
    @Mapping(target = "adDesign", ignore = true)     // @AfterMapping
    @Mapping(target = "installerUser", ignore = true)     // @AfterMapping
    ReservationAdSpace publicToDomain(PublicReservationAdSpace source, @Context EntityManager entityManager);

    @Mapping(target = "adSpaceId", source = "adSpace.id")
    @Mapping(target = "adDesignId", source = "adDesign.id")
    @Mapping(target = "reservationId", source = "reservation.id")
    @Mapping(target = "installerUserId", source = "installerUser.id")
    @Mapping(target = "installerUserFirstName", source = "installerUser.firstName")
    @Mapping(target = "installerUserLastName", source = "installerUser.lastName")
    PublicReservationAdSpace domainToPublic(ReservationAdSpace source);

    @Mapping(target = "adSpaceId", source = "adSpace.id")
    @Mapping(target = "adDesignId", source = "adDesign.id")
    @Mapping(target = "reservationId", source = "reservation.id")
    @Mapping(target = "installerUserId", source = "installerUser.id")
    @Mapping(target = "installerUserFirstName", source = "installerUser.firstName")
    @Mapping(target = "installerUserLastName", source = "installerUser.lastName")
    List<PublicReservationAdSpace> domainToPublicList(List<ReservationAdSpace> sourceList);

    @AfterMapping
    default void afterMappingToDomain(PublicReservationAdSpace source, @MappingTarget ReservationAdSpace target,
                                      @Context EntityManager entityManager) {
        AdSpace adSpace = entityManager.find(AdSpace.class, source.getAdSpaceId());
        target.setAdSpace(adSpace);

        User user = entityManager.find(User.class, source.getInstallerUserId());
        target.setInstallerUser(user);

        if (source.getReservationId() != null) {
            // ReservationId is missing if the Reservation with its ReservationAdSpaces is being saved for the 1st time.
            Reservation reservation = entityManager.find(Reservation.class, source.getReservationId());
            target.setReservation(reservation);
        }
        if (source.getAdDesignId() != null) {
            // AdDesignId is missing if the Reservation with its ReservationAdSpaces is being saved for the 1st time.
            AdDesign adDesign = entityManager.find(AdDesign.class, source.getAdDesignId());
            target.setAdDesign(adDesign);
        }
    }

}
