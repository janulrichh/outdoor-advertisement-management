package com.admanagement.app.presentation.mappers;

import com.admanagement.app.persistence.model.User;
import com.admanagement.app.persistence.model.UserRole;
import com.admanagement.app.presentation.dtos.PublicUser;
import jakarta.persistence.EntityManager;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    // PUBLIC DTO: PublicUser
    // DOMAIN CLASS: User

    @Mapping(target = "clients", ignore = true)
    @Mapping(target = "userRole", ignore = true)    // @AfterMapping
    @Mapping(target = "reservations", ignore = true)    // ignore input
    @Mapping(target = "reservationAdSpaces", ignore = true)     // ignore input
    @Mapping(target = "adDesignOrders", ignore = true)  // ignore input
    User publicToDomain(PublicUser source, @Context EntityManager entityManager);

    @Mapping(target = "id", source = "source.id")
    @Mapping(target = "password", ignore = true)    // do not expose
    @Mapping(target = "userRoleId", source = "source.userRole.id")
    PublicUser domainToPublic(User source);

    @Mapping(target = "id", source = "source.id")
    @Mapping(target = "password", ignore = true)    // do not expose
    @Mapping(target = "userRoleId", source = "source.userRole.id")
    List<PublicUser> domainToPublicList(List<User> sourceList);

    @AfterMapping
    default void afterMappingToDomain(PublicUser source, @MappingTarget User target,
                                       @Context EntityManager entityManager) {
        target.setUserRole(entityManager.find(UserRole.class, source.getUserRoleId()));
    }

}
