package com.admanagement.app.presentation.mappers;

import com.admanagement.app.persistence.model.AdLocation;
import com.admanagement.app.persistence.model.AssetGroup;
import com.admanagement.app.persistence.model.BaseEntity;
import com.admanagement.app.presentation.dtos.PublicAdLocation;
import com.admanagement.app.service.utils.GeometryUtility;
import jakarta.persistence.EntityManager;
import org.mapstruct.*;

import java.util.List;


@Mapper(
        componentModel = "spring",
        uses = {AssetGroupMapper.class, AdSpaceMapper.class},
        imports = GeometryUtility.class
)
public interface AdLocationMapper {

    // PUBLIC DTO: PublicAdLocation
    // DOMAIN CLASS: AdLocation

    @Mapping(target = "adSpaces", ignore = true)    // ignore input
    @Mapping(target = "position", expression = "java(GeometryUtility.toPoint(source.getLatitude(), source.getLongitude()))")
    AdLocation publicToDomain(PublicAdLocation source, @Context EntityManager entityManager);

    @Mapping(target = "latitude", source = "source.position.x")
    @Mapping(target = "longitude", source = "source.position.y")
    @Mapping(target = "adSpaceIds", ignore = true)     // @BeforeMapping
    @Mapping(target = "assetGroupIds", ignore = true)     // @BeforeMapping
    PublicAdLocation domainToPublic(AdLocation source);

    @Mapping(target = "latitude", source = "source.position.x")
    @Mapping(target = "longitude", source = "source.position.y")
    @Mapping(target = "adSpaceIds", ignore = true)     // @BeforeMapping
    @Mapping(target = "assetGroupIds", ignore = true)     // @BeforeMapping
    List<PublicAdLocation> domainToPublicList(List<AdLocation> sourceList);

    @AfterMapping
    default void afterMappingToDomain(PublicAdLocation source, @MappingTarget AdLocation target,
                                       @Context EntityManager entityManager) {
        List<AssetGroup> assetGroups = source.getAssetGroupIds().stream()
                .map(assetGroupId -> entityManager.find(AssetGroup.class, assetGroupId)).toList();
        target.setAssetGroups(assetGroups);
    }

    @BeforeMapping
    default void beforeMappingToPublic(AdLocation source, @MappingTarget PublicAdLocation target) {
        List<Integer> adSpaceIds = source.getAdSpaces().stream().map(BaseEntity::getId).toList();
        target.setAdSpaceIds(adSpaceIds);
        List<Integer> assetGroupIds = source.getAssetGroups().stream().map(BaseEntity::getId).toList();
        target.setAssetGroupIds(assetGroupIds);
    }
}

