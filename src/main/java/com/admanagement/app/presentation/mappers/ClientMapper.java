package com.admanagement.app.presentation.mappers;

import com.admanagement.app.persistence.model.BaseEntity;
import com.admanagement.app.persistence.model.Client;
import com.admanagement.app.persistence.model.User;
import com.admanagement.app.presentation.dtos.PublicClient;
import jakarta.persistence.EntityManager;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = ReservationShallowMapper.class)
public interface ClientMapper {

    // PUBLIC DTO: PublicClient
    // DOMAIN CLASS: Client

    @Mapping(target = "creatorUser", ignore = true)     // @AfterMapping
    @Mapping(target = "reservations", ignore = true)    // ignore input
    Client publicToDomain(PublicClient source, @Context EntityManager entityManager);

    @Mapping(target = "creatorUserId", source = "creatorUser.id")
    @Mapping(target = "creatorUserFirstName", source = "creatorUser.firstName")
    @Mapping(target = "creatorUserLastName", source = "creatorUser.lastName")
    @Mapping(target = "reservationIds", ignore = true)  // @BeforeMapping
    PublicClient domainToPublic(Client source);

    @Mapping(target = "creatorUserId", source = "creatorUser.id")
    @Mapping(target = "creatorUserFirstName", source = "creatorUser.firstName")
    @Mapping(target = "creatorUserLastName", source = "creatorUser.lastName")
    @Mapping(target = "reservationIds", ignore = true)  // @BeforeMapping
    List<PublicClient> domainToPublicList(List<Client> sourceList);

    @AfterMapping
    default void afterMappingToDomain(PublicClient source, @MappingTarget Client target,
                                       @Context EntityManager entityManager) {
        User user = entityManager.find(User.class, source.getCreatorUserId());
        target.setCreatorUser(user);
    }

    @BeforeMapping
    default void beforeMappingToPublic(Client source, @MappingTarget PublicClient target) {
        List<Integer> reservationIds = source.getReservations().stream().map(BaseEntity::getId).toList();
        target.setReservationIds(reservationIds);
    }
}
