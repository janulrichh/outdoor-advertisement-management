package com.admanagement.app.presentation.mappers;

import com.admanagement.app.persistence.model.*;
import com.admanagement.app.presentation.dtos.PublicAdSpace;
import jakarta.persistence.EntityManager;
import org.mapstruct.*;

import java.util.List;


@Mapper(componentModel = "spring", uses = {AssetGroupMapper.class, AdLocationMapper.class})
public interface AdSpaceMapper {

    // PUBLIC: PublicAdSpace
    // DOMAIN: AdSpace

    @Mapping(target = "reservationAdSpaces", ignore = true)     // ignore input
    @Mapping(target = "image", ignore = true)   // ignore input
    @Mapping(target = "imageFileExtension", ignore = true)   // ignore input
    @Mapping(target = "imageFileName", ignore = true)   // ignore input
    @Mapping(target = "imageUploadTime", ignore = true)   // ignore input
    AdSpace publicToDomain(PublicAdSpace source, @Context EntityManager entityManager);

    @Mapping(target = "adLocationId", source = "adLocation.id")
    @Mapping(target = "reservationAdSpaceIds", ignore = true)   // @BeforeMapping
    @Mapping(target = "assetGroupIds", ignore = true)       // @Beforemapping
    PublicAdSpace domainToPublic(AdSpace source);

    @Mapping(target = "adLocationId", source = "adLocation.id")
    @Mapping(target = "reservationAdSpaceIds", ignore = true)   // @BeforeMapping
    @Mapping(target = "assetGroupIds", ignore = true)       // @Beforemapping
    List<PublicAdSpace> domainToPublicList(List<AdSpace> sourceList);

    @AfterMapping
    default void afterMappingToDomain(PublicAdSpace source, @MappingTarget AdSpace target,
                                       @Context EntityManager entityManager) {
        target.setAdLocation(entityManager.find(AdLocation.class, source.getAdLocationId()));
        List<AssetGroup> assetGroups = source.getAssetGroupIds().stream()
                .map(assetGroupId -> entityManager.find(AssetGroup.class, assetGroupId)).toList();
        target.setAssetGroups(assetGroups);
    }

    @BeforeMapping
    default void beforeMappingToPublic(AdSpace source, @MappingTarget PublicAdSpace target) {
        List<Integer> reservationAdSpaceIds = source.getReservationAdSpaces().stream().map(BaseEntity::getId).toList();
        target.setReservationAdSpaceIds(reservationAdSpaceIds);
        List<Integer> assetGroupIds = source.getAssetGroups().stream().map(BaseEntity::getId).toList();
        target.setAssetGroupIds(assetGroupIds);
    }
}
