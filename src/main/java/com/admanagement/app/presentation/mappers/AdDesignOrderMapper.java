package com.admanagement.app.presentation.mappers;


import com.admanagement.app.persistence.model.AdDesignOrder;
import com.admanagement.app.presentation.dtos.PublicAdDesignOrder;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AdDesignOrderMapper {

    // PUBLIC DTO: PublicAdDesignOrder
    // DOMAIN CLASS: AdDesignOrder

    @Mapping(target = "adDesignId", source = "adDesign.id")
    @Mapping(target = "orderUserId", source = "orderUser.id")
    @Mapping(target = "orderUserFirstName", source = "orderUser.firstName")
    @Mapping(target = "orderUserLastName", source = "orderUser.lastName")
    PublicAdDesignOrder domainToPublic(AdDesignOrder source);

    @Mapping(target = "adDesignId", source = "adDesign.id")
    @Mapping(target = "orderUserId", source = "orderUser.id")
    @Mapping(target = "orderUserFirstName", source = "orderUser.firstName")
    @Mapping(target = "orderUserLastName", source = "orderUser.lastName")
    List<PublicAdDesignOrder> domainToPublicList(List<AdDesignOrder> source);


}
