package com.admanagement.app.presentation.mappers;

import com.admanagement.app.persistence.model.AdDesign;
import com.admanagement.app.persistence.model.AdDesignOrder;
import com.admanagement.app.persistence.model.BaseEntity;
import com.admanagement.app.presentation.dtos.PublicAdDesign;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = AdDesignOrderMapper.class)
public interface AdDesignMapper {

    // PUBLIC DTO: PublicAdDesignDetails
    // DOMAIN CLASS: AdDesign

    @Mapping(target = "reservationId", source = "reservation.id")
    @Mapping(target = "reservationAdSpaceIds", ignore = true)       // @BeforeMapping
    @Mapping(target = "numberOfReservationAdSpaces", ignore = true)     // @BeforeMapping
    @Mapping(target = "numberOfPrintsOrdered", ignore = true)       // @BeforeMapping
    PublicAdDesign domainToPublic(AdDesign source);

    @Mapping(target = "reservationId", source = "reservation.id") //
    @Mapping(target = "reservationAdSpaceIds", ignore = true)       // @BeforeMapping
    @Mapping(target = "numberOfReservationAdSpaces", ignore = true)     // @BeforeMapping
    @Mapping(target = "numberOfPrintsOrdered", ignore = true)       // @BeforeMapping
    List<PublicAdDesign> domainToPublicList(List<AdDesign> source);

    @BeforeMapping
    default void beforeMappingToPublic(AdDesign source, @MappingTarget PublicAdDesign target) {
        List<Integer> reservationAdSpaceIds = source.getReservationAdSpaces().stream().map(BaseEntity::getId).toList();
        target.setReservationAdSpaceIds(reservationAdSpaceIds);
        target.setNumberOfReservationAdSpaces(reservationAdSpaceIds.size());

        Integer numberOfPrintsOrdered = source.getAdDesignOrders().stream().mapToInt(AdDesignOrder::getOrderAmount).sum();
        target.setNumberOfPrintsOrdered(numberOfPrintsOrdered);
    }
}
