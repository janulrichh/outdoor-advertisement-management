package com.admanagement.app.presentation.mappers;

import com.admanagement.app.persistence.model.BaseEntity;
import com.admanagement.app.persistence.model.Reservation;
import com.admanagement.app.presentation.dtos.PublicReservationShallow;
import org.mapstruct.*;

import java.util.List;

@Mapper(componentModel = "spring", uses = ReservationAdSpaceMapper.class)
public interface ReservationShallowMapper {

    // PUBLIC DTO: PublicReservationShallow
    // DOMAIN CLASS: Reservation

    @Mapping(target = "clientId", source = "client.id")
    @Mapping(target = "clientName", source = "client.name")
    @Mapping(target = "creatorUserId", source = "creatorUser.id")
    @Mapping(target = "creatorUserFirstName", source = "creatorUser.firstName")
    @Mapping(target = "creatorUserLastName", source = "creatorUser.lastName")
    @Mapping(target = "adDesignIds", ignore = true)     // @BeforeMapping
    @Mapping(target = "reservationAdSpaceIds", ignore = true)     // @BeforeMapping
    PublicReservationShallow domainToPublic(Reservation source);

    @BeforeMapping
    default void beforeMappingToPublic(Reservation source, @MappingTarget PublicReservationShallow target) {
        List<Integer> adDesignIds = source.getAdDesigns().stream().map(BaseEntity::getId).toList();
        target.setAdDesignIds(adDesignIds);
        List<Integer> reservationAdSpaceIds = source.getReservationAdSpaces().stream().map(BaseEntity::getId).toList();
        target.setReservationAdSpaceIds(reservationAdSpaceIds);
    }
}
