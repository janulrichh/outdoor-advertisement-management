package com.admanagement.app.presentation.dtos.errors;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.util.List;

@Getter
@AllArgsConstructor
@RequiredArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValidationError {

    private String field;

    private String code;

    private List<String> arguments;

    private String message;

}
