package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.Id;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PublicBaseEntity {

    @Id
    @Schema(description = "Unique identifier. Automatically generated in dynamic tables.")
    private Integer id;

}
