package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Schema(description = "AdLocation response/request DTO")
public class PublicAdLocation extends PublicBaseEntity {

    @NotNull
    @Size(max = 10)
    @Schema(description = "Enumeration used for differentiation of ad spaces outside the database.", example = "#ABC01")
    private String number;

    @Schema(description = "Status of the ad location." +
            " When not provided, keeps existing value when updating or sets to 'true' when adding a new entity.")
    private Boolean enabled;

    @Size(max = 50)
    @Schema(description = "Name of the bus stop where the adLocation is positioned (if applicable).", example = "Bussijaam")
    private String stopName;

    @Size(max = 50)
    @Schema(description = "Name of the street where the adLocation is positioned (if applicable).", example = "Pargi tn")
    private String streetName;

    @Schema(description = "Designation of the transit route direction for differentiation between multiple adLocations with the same 'stopName' (if applicable). Take as an example, a bus stop with one name with shelters on both sides of the road.", example = "0")
    private Character direction;

    @Size(max = 3000)
    @Schema(description = "Field for additional notes.", example = "Example text.")
    private String comment;

    @Schema(description = "List of ids of AdSpaces that are positioned at this adLocation. Used only in response.")
    private List<Integer> adSpaceIds;

    @Schema(description = "List of AssetGroups to which the adLocation belongs. Used only in response.")
    private List<PublicAssetGroup> assetGroups;
    @Schema(description = "List of ids of AssetGroups to which the adLocation belongs.")
    private List<Integer> assetGroupIds;

    @Schema(description = "Latitude of the adLocation position.", example = "59.43703300279")
    private Double latitude;
    @Schema(description = "Longitude of the adLocation position.", example = "24.753552397362")
    private Double longitude;

}
