package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@Schema(description = "Request DTO for AdDesign print ordering")
public class PublicAdDesignPrintOrderRequest {

    @NotNull
    @Schema(description = "Ids and amounts of AdDesign prints to order.", example = "{ 1: 5, 2: 5 }")
    private Map<Integer, Integer> adDesignAmounts;
}
