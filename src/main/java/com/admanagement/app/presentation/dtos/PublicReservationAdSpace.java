package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Schema(description = "ReservationAdSpace response/request DTO. " +
        "ReservationAdSpace indicates that a AdSpace is included in a Reservation.")
public class PublicReservationAdSpace extends PublicBaseEntity {

    @NotNull
    @Schema(description = "Id of AdSpace that is involved in the reservation.", example = "1")
    private Integer adSpaceId;

    @Schema(description = "Id of Reservation that includes the adSpace. " +
            "Not required in request as is always being sent together with Reservation", example = "1")
    private Integer reservationId;

    @Schema(description = "AdDesign id that is used on the adSpace in the reservation. If not provided, assigns a generated adDesign", example = "1")
    private Integer adDesignId;

    @Schema(description = "User account id of the user who has most recently updated the installation status (adInstalledAt). Initializes with the creator user account. Used only in response.", example = "1")
    private Integer installerUserId;
    @Schema(description = "First name of the user who has most recently updated the installation status (adInstalledAt). Initializes with the creator user account. Used only in response.", example = "1")
    private String installerUserFirstName;
    @Schema(description = "Last name of the user who has most recently updated the installation status (adInstalledAt). Initializes with the creator user account. Used only in response.", example = "1")
    private String installerUserLastName;

    @NotNull
    @Schema(description = "Starting time of the adSpace involvement in the reservation.", example = "2022-03-01T10:30:15Z")
    private String fromTime;

    @NotNull
    @Schema(description = "End time of the adSpace involvement in the reservation.", example = "2022-03-01T10:30:15Z")
    private String toTime;

    @Schema(description = "Time of ad design installation time on the involved adSpace. 'null' when not installed", example = "2022-03-01T10:30:15Z")
    private String adInstalledAt;
}

