package com.admanagement.app.presentation.dtos;

import com.admanagement.app.persistence.model.AssetGroupType;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Schema(description = "AssetGroup response/request DTO")
public class PublicAssetGroup extends PublicBaseEntity {

    @NotNull
    @Enumerated(EnumType.STRING)
    @Schema(description = "Asset group type. Possible types: 'AD_LOCATION', 'AD_SPACE', 'COMBINED'.", example = "AD_SPACE")
    private AssetGroupType assetGroupType;

    @NotNull
    @Size(max = 50)
    @Schema(description = "Asset group name.", example = "Bussiootepaviljonid")
    private String name;

    @Size(max = 255)
    @Schema(description = "Asset group description.", example = "Example text.")
    private String description;

    @Schema(description = "IDs of ad locations in the asset group. Used only in response.")
    private List<Integer> adLocationIds = new ArrayList<>();

    @Schema(description = "IDs of ad spaces in the asset group. Used only in response.")
    private List<Integer> adSpaceIds = new ArrayList<>();

}
