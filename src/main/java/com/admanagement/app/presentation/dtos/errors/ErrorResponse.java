package com.admanagement.app.presentation.dtos.errors;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ErrorResponse {

    private List<String> parameterErrors = new ArrayList<>();

    private List<ValidationError> validationErrors = new ArrayList<>();

    private List<String> constraintViolationErrors = new ArrayList<>();

}
