package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Schema(description = "AdDesignOrder response DTO")
public class PublicAdDesignOrder extends PublicBaseEntity {

    @NotNull
    @Schema(description = "User account id of the order creator.", example = "1")
    private Integer orderUserId;
    @Schema(description = "First name of the order creator.", example = "Eesnimi")
    private String orderUserFirstName;
    @Schema(description = "Last name of the order creator.", example = "Perenimi")
    private String orderUserLastName;

    @NotNull
    @Schema(description = "Id of AdDesign that was ordered.", example = "1")
    private Integer adDesignId;

    @NotNull
    @Schema(description = "Order creation time.", example = "2022-03-01T10:30:15Z")
    private String orderedAt;

    @NotNull
    @Schema(description = "Number of AdDesign print copies ordered with the order.", example = "5")
    private Integer orderAmount;
}
