package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Schema(description = "Reservation response/request DTO")
public class PublicReservation extends PublicBaseEntity {

    @NotNull
    @Schema(description = "Id of client for whom the Reservation is made.", example = "1")
    private Integer clientId;
    @Schema(description = "Name of client for whom the Reservation is made. Used only in response.", example = "OÜ Ettevõte")
    private String clientName;

    @Schema(description = "User account id of the client profile creator. Used only in response.", example = "1")
    private Integer creatorUserId;
    @Schema(description = "First name of the client profile creator. Used only in response.", example = "Eesnimi")
    private String creatorUserFirstName;
    @Schema(description = "Last name of the client profile creator. Used only in response.", example = "Perenimi")
    private String creatorUserLastName;

    @NotNull
    @Schema(description = "Name of the advertisement campaign for which the reservation is made.", example = "Reklaamkampaania")
    private String campaignName;

    @NotNull
    @Schema(description = "Cancellation status of the reservation.")
    private Boolean cancelled;

    @Schema(description = "Creation time. Used only in response.", example = "2022-03-01T10:30:15Z")
    private String createdAt;

    @Schema(description = "Approval time. 'null' if not approved.", example = "2022-03-01T10:30:15Z")
    private String approvedAt;

    @Size(max = 30)
    @Schema(description = "Number of invoice issued for the reservation.", example = "ABC-001-2024")
    private String invoiceNumber;

    @Size(max = 3000)
    @Schema(description = "Field for additional notes.", example = "Example text.")
    private String comment;

    @Schema(description = "List of adDesign ids uploaded for this Reservation. Used only in response.")
    private List<Integer> adDesignIds = new ArrayList<>();

    @Schema(description = "List of reservationAdSpaces.")
    private List<PublicReservationAdSpace> reservationAdSpaces = new ArrayList<>();

    public void fillIdsInConnectedEntities(Integer creatorUserId) {
        for (PublicReservationAdSpace ras : reservationAdSpaces) {
            ras.setReservationId(getId());
            ras.setInstallerUserId(creatorUserId);
        }
    }
}