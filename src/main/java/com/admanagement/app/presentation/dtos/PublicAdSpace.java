package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@Schema(description = "AdSpace response/request DTO")
public class PublicAdSpace extends PublicBaseEntity {

    @NotNull
    @Schema(description = "Id of adLocation where the adSpace is located.", example = "1")
    private Integer adLocationId;
    @Schema(description = "AdLocation where the adSpace is located. Used only in response.")
    private PublicAdLocation adLocation;

    @NotNull
    @Size(max = 50)
    @Schema(description = "Specification of the position inside the adLocation where adSpace is located.", example = "Interior wall.")
    private String locator;

    @Schema(description = "Status of the adSpace." +
            " When not provided, keeps existing value when updating or sets to 'true' when adding a new entity.")
    private Boolean enabled;

    @Size(max = 3000)
    @Schema(description = "Field for additional notes.", example = "Example text.")
    private String comment;

    @Schema(description = "List of ids of reservationAdSpaces that represent the involvement of the adSpace in a reservation. Used only in response.")
    private List<Integer> reservationAdSpaceIds;

    @Schema(description = "List of AssetGroups to which the adSpace belongs. Used only in response.")
    private List<PublicAssetGroup> assetGroups;
    @Schema(description = "List of ids of AssetGroups to which the adSpace belongs.")
    private List<Integer> assetGroupIds;
}

