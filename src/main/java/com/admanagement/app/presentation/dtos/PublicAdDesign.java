package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Schema(description = "AdDesign response DTO (without image byte-stream)")
public class PublicAdDesign extends PublicBaseEntity {

    @Schema(description = "Image file presence indicator")
    private boolean imageFileExists;

    @Schema(description = "Id of the Reservation that uses this adDesign")
    private Integer reservationId;

    @Schema(description = "Public id of the adDesign for sharing the image file with printing service provider. Generated after the design is ordered.")
    private String publicId;

    @Schema(description = "Ids of ReservationAdSpaces that use this adDesign")
    private List<Integer> reservationAdSpaceIds;

    @Schema(description = "Number of ReservationAdSpaces that use this adDesign, i.e number of copies to be ordered.", example = "5")
    private Integer numberOfReservationAdSpaces;

    @Schema(description = "AdDesignOrders that have been made for this AdDesign.")
    private List<PublicAdDesignOrder> adDesignOrders = new ArrayList<>();

    @Schema(description = "Total number of AdDesign prints ordered in AdDesignOrders.", example = "5")
    private Integer numberOfPrintsOrdered;

}
