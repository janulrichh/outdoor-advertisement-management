package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@EqualsAndHashCode(callSuper = false)
@Schema(description = "Client profile response/request DTO")
public class PublicClient extends PublicBaseEntity {

    @Schema(description = "User account id of the client profile creator. Used only in response.", example = "1")
    private Integer creatorUserId;
    @Schema(description = "First name of the client profile creator. Used only in response.", example = "Eesnimi")
    private String creatorUserFirstName;
    @Schema(description = "Last name of the client profile creator. Used only in response.", example = "Perenimi")
    private String creatorUserLastName;

    @Size(max = 128)
    @Schema(description = "Client name.", example = "OÜ Ettevõte")
    private String name;

    @Size(max = 128)
    @Schema(description = "Client (representative) person name.", example = "Eesnimi Perenimi")
    private String contactPersonName;

    @Size(max = 20)
    @Schema(description = "Client contact phone number.", example = "+372 55 555 555")
    private String contactPhone;

    @Email
    @Size(max = 255)
    @Schema(description = "Client contact email address.", example = "nimi@ettevote.ee")
    private String email;

    @Schema(description = "Status of the client profile." +
            " When not provided, keeps existing value when updating or sets to 'true' when adding a new entity.")
    private Boolean enabled;

    @Schema(description = "Creation time. Used only in response.", example = "2022-03-01T10:30:15Z")
    private String createdAt;

    @Size(max = 3000)
    @Schema(description = "Field for additional notes.", example = "Example text.")
    private String comment;

    @Schema(description = "List of Client's Reservations. Used only in response.")
    private List<PublicReservationShallow> reservations = new ArrayList<>();
    @Schema(description = "List of ids of Client's Reservations. Used only in response.")
    private List<Integer> reservationIds = new ArrayList<>();
}
