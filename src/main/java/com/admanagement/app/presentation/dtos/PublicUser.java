package com.admanagement.app.presentation.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Schema(description = "User account response/request DTO")
public class PublicUser extends PublicBaseEntity {

    @NotNull
    @NotBlank
    @Size(min = 3, max = 255)
    @Schema(description = "Username of the user account.", example = "kasutaja")
    private String username;

    @NotNull
    @NotBlank
    @Size(max = 255)
    @Schema(description = "Password of the user account in plain text. Only used in request.", example = "secret")
    private String password;

    @Schema(description = "Status of the user account." +
            " When not provided, keeps existing value when updating or sets to 'true' when adding a new entity.")
    private final Boolean enabled = true;

    @NotNull
    @NotBlank
    @Size(max = 50)
    @Schema(description = "First name of the user account owner.", example = "Eesnimi")
    private String firstName;

    @NotNull
    @NotBlank
    @Size(max = 50)
    @Schema(description = "Last name of the user account owner.", example = "Perenimi")
    private String lastName;

    @Schema(description = "Creation time. Used only in response.", example = "2022-03-01T10:30:15Z")
    private String createdAt;

    @NotNull
    @Schema(description = "User role id of the user account.", example = "1")
    private Integer userRoleId;

}
