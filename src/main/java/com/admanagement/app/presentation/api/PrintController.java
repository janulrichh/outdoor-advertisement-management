package com.admanagement.app.presentation.api;

import com.admanagement.app.persistence.model.AdDesign;
import com.admanagement.app.persistence.model.User;
import com.admanagement.app.presentation.api.utils.AuthenticationUserResolver;
import com.admanagement.app.presentation.dtos.PublicAdDesign;
import com.admanagement.app.presentation.dtos.PublicAdDesignPrintOrderRequest;
import com.admanagement.app.presentation.dtos.PublicPrintServiceAddress;
import com.admanagement.app.presentation.mappers.AdDesignMapper;
import com.admanagement.app.service.services.PrintOrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.mail.MessagingException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/print")
@SecurityRequirement(name = "Bearer Authentication")
@Tag(name = "AdDesign Printing", description = "Print order service for AdDesigns.")
public class PrintController {

    private final AuthenticationUserResolver authenticationUserResolver;
    private final PrintOrderService printOrderService;
    private final AdDesignMapper mapper;

    public PrintController(AuthenticationUserResolver authenticationUserResolver, PrintOrderService printOrderService, AdDesignMapper mapper) {
        this.authenticationUserResolver = authenticationUserResolver;
        this.printOrderService = printOrderService;
        this.mapper = mapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Get AdDesigns that must be printed to install.")
    public ResponseEntity<List<PublicAdDesign>> getUnOrderedAdDesigns() {
        List<AdDesign> adDesigns = printOrderService.findAllWaitingToOrder();
        List<PublicAdDesign> publicAdDesignDetails = mapper.domainToPublicList(adDesigns);
        return ResponseEntity.ok().body(publicAdDesignDetails);
    }

    @GetMapping(value = "/order/{publicId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @Operation(summary = "Download publicly accessible ordered AdDesign image.", description = "Used in order e-mails.")
    public ResponseEntity<?> getOrderImage(@PathVariable String publicId) {
        Optional<AdDesign> adDesign = printOrderService.findAdDesignByPublicId(publicId);
        if (adDesign.isEmpty() || !adDesign.get().getImageFileExists())
            return ResponseEntity.notFound().build();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentDispositionFormData("image", adDesign.get().getFileNameWithExtension());
        return ResponseEntity.ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_OCTET_STREAM).
                body(adDesign.get().getImage());
    }

    @PostMapping("/send")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Create order of AdDesigns in specified amounts from print service provider.")
    public ResponseEntity<Void> sendAdDesignsToPrint(@RequestBody @Valid PublicAdDesignPrintOrderRequest publicAdDesignPrintOrderRequest,
                                                  Authentication auth, HttpServletRequest request) throws MessagingException {
        User orderUser = authenticationUserResolver.getUserByAuth(auth);
        printOrderService.orderAdDesigns(publicAdDesignPrintOrderRequest.getAdDesignAmounts(), orderUser, getRequestBaseUrl(request));
        return ResponseEntity.ok().build();
    }

    @PutMapping("/address")
    @PreAuthorize("hasRole('ADMIN')")
    @Operation(summary = "Change e-mail address of print service provider (order recipient).")
    public ResponseEntity<Void> updateAddress(@RequestBody @Valid PublicPrintServiceAddress email) {
        printOrderService.updateAddress(email);
        return ResponseEntity.ok().build();
    }

    private String getRequestBaseUrl(HttpServletRequest request) {
        String requestFullUrl = request.getRequestURL().toString();
        int endIndex = requestFullUrl.length() - request.getRequestURI().length();
        return requestFullUrl.substring(0, endIndex);
    }
}
