package com.admanagement.app.presentation.api;

import com.admanagement.app.persistence.model.AssetGroup;
import com.admanagement.app.presentation.dtos.PublicAssetGroup;
import com.admanagement.app.presentation.mappers.AssetGroupMapper;
import com.admanagement.app.service.services.AssetGroupService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/assetgroups")
@SecurityRequirement(name = "Bearer Authentication")
@Tag(name = "AssetGroups", description = "Asset Group Management")
public class AssetGroupController {

    private final AssetGroupService service;
    private final AssetGroupMapper mapper;
    private final EntityManager em;

    public AssetGroupController(AssetGroupService service, AssetGroupMapper mapper, EntityManager em) {
        this.service = service;
        this.mapper = mapper;
        this.em = em;
    }

    @GetMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Get all asset groups.")
    public ResponseEntity<List<PublicAssetGroup>> getAssetGroups() {
        List<AssetGroup> assetGroups = service.findAll();
        List<PublicAssetGroup> publicAssetGroups = mapper.domainToPublicList(assetGroups);
        return ResponseEntity.ok().body(publicAssetGroups);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Get asset group by id.")
    public ResponseEntity<PublicAssetGroup> getAssetGroup(@PathVariable Integer id) {
        Optional<AssetGroup> assetGroup = service.findById(id);
        if (assetGroup.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        PublicAssetGroup publicAssetGroup = mapper.domainToPublic(assetGroup.get());
        return ResponseEntity.ok(publicAssetGroup);
    }

    @PostMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Add a new asset group.")
    public ResponseEntity<PublicAssetGroup> addAssetGroup(@RequestBody @Valid PublicAssetGroup publicAssetGroup) {
        AssetGroup assetGroup = mapper.publicToDomain(publicAssetGroup, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.save(assetGroup)));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Update asset group by id.")
    public ResponseEntity<PublicAssetGroup> updateAssetGroup(@PathVariable Integer id, @RequestBody @Valid PublicAssetGroup publicAssetGroup) {
        if (service.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        publicAssetGroup.setId(id);
        AssetGroup assetGroup = mapper.publicToDomain(publicAssetGroup, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.update(assetGroup, id)));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete asset group by id.")
    public void deleteClient(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
