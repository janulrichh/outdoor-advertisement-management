package com.admanagement.app.presentation.api.utils;

import com.admanagement.app.persistence.model.User;
import com.admanagement.app.service.services.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AuthenticationUserResolver {

    private final UserService userService;

    public AuthenticationUserResolver(UserService userService) {
        this.userService = userService;
    }

    public User getUserByAuth(Authentication auth) {
        Optional<User> authUser = userService.findByUsername(auth.getName());
        if (authUser.isEmpty()) {
            throw new RuntimeException("Auth user detection failure");
        }
        return authUser.get();
    }
}
