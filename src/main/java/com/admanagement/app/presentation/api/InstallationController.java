package com.admanagement.app.presentation.api;

import com.admanagement.app.persistence.model.Reservation;
import com.admanagement.app.persistence.model.User;
import com.admanagement.app.presentation.api.utils.AuthenticationUserResolver;
import com.admanagement.app.presentation.dtos.PublicReservation;
import com.admanagement.app.presentation.mappers.ReservationMapper;
import com.admanagement.app.service.services.ReservationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/install")
@SecurityRequirement(name = "Bearer Authentication")
@Tag(name = "AdDesign Installation", description = "Installation tracking for INSTALL role.")
public class InstallationController {

    private final AuthenticationUserResolver authenticationUserResolver;
    private final ReservationService service;
    private final ReservationMapper mapper;

    public InstallationController(AuthenticationUserResolver authenticationUserResolver, ReservationService service, ReservationMapper mapper) {
        this.authenticationUserResolver = authenticationUserResolver;
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('INSTALL')")
    @Operation(summary = "Get Reservations with ReservationAdSpaces that require the installation of AdDesign.",
            description = "Get results in all areas when no parameters provided")
    public ResponseEntity<List<PublicReservation>> getReservationsToInstall(
            @Parameter(name = "Search area center point latitude") @RequestParam(required = false) Double latitude,
            @Parameter(name = "Search area center point longitude") @RequestParam(required = false) Double longitude,
            @Parameter(name = "Search area radius in meters") @RequestParam(required = false) Double radius) {
        List<Reservation> matchingReservations = service.findReservationsThatRequireAdDesignInstallation(latitude, longitude, radius);
        List<PublicReservation> publicMatchingAdSpaces = mapper.domainToPublicList(matchingReservations);
        return ResponseEntity.ok(publicMatchingAdSpaces);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('INSTALL')")
    @Operation(summary = "Update ReservationAdSpace installation status to installed.")
    public ResponseEntity<Void> updateInstallationStatusOfReservationAdSpace(@Parameter(description = "ReservationAdSpace id") @PathVariable Integer id, Authentication auth) {
        if (service.findReservationAdSpaceById(id).isEmpty())
            return ResponseEntity.notFound().build();
        User installerUser = authenticationUserResolver.getUserByAuth(auth);
        service.updateInstallationStatusOfReservationAdSpace(id, installerUser);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('INSTALL')")
    @Operation(summary = "Reverse ReservationAdSpace installation status.")
    public ResponseEntity<Void> reverseInstallationStatusOfReservationAdSpace(@Parameter(description = "ReservationAdSpace id") @PathVariable Integer id) {
        if (service.findReservationAdSpaceById(id).isEmpty())
            return ResponseEntity.notFound().build();
        service.reverseInstallationStatusOfReservationAdSpace(id);
        return ResponseEntity.ok().build();
    }

}
