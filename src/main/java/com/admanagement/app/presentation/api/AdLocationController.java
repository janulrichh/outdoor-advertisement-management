package com.admanagement.app.presentation.api;

import com.admanagement.app.persistence.model.AdLocation;

import com.admanagement.app.presentation.dtos.PublicAdLocation;
import com.admanagement.app.presentation.mappers.AdLocationMapper;
import com.admanagement.app.service.services.AdLocationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/adlocations")
@SecurityRequirement(name = "Bearer Authentication")
@Tag(name = "AdLocations", description = "AdLocation Management")
public class AdLocationController {

    private final AdLocationService service;
    private final AdLocationMapper mapper;
    private final EntityManager em;

    public AdLocationController(AdLocationService service, AdLocationMapper mapper, EntityManager em) {
        this.service = service;
        this.mapper = mapper;
        this.em = em;
    }

    @GetMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Get adLocations that match search parameters. (Get all adLocations)",
            description = "Get all adLocations when no parameters provided. Does not include adLocations that are disabled.")
    public ResponseEntity<List<PublicAdLocation>> getAdLocations(@Parameter(name = "Search area center point latitude") @RequestParam(required = false) Double latitude,
                                                                 @Parameter(name = "Search area center point longitude") @RequestParam(required = false) Double longitude,
                                                                 @Parameter(name = "Search area radius in meters") @RequestParam(required = false) Double radius) {
        List<AdLocation> adLocations = service.findMatching(latitude, longitude, radius);
        List<PublicAdLocation> publicAdLocations = mapper.domainToPublicList(adLocations);
        return ResponseEntity.ok().body(publicAdLocations);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Get adLocation by id.")
    public ResponseEntity<PublicAdLocation> getAdLocation(@PathVariable Integer id) {
        Optional<AdLocation> adLocation = service.findById(id);
        if (adLocation.isEmpty())
            return ResponseEntity.notFound().build();
        PublicAdLocation publicAdLocation = mapper.domainToPublic(adLocation.get());
        return ResponseEntity.ok(publicAdLocation);
    }

    @PostMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Add a new adLocation.")
    public ResponseEntity<PublicAdLocation> addAdLocation(@RequestBody @Valid PublicAdLocation publicAdLocation) {
        AdLocation adLocation = mapper.publicToDomain(publicAdLocation, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.save(adLocation)));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Update adLocation by id.")
    public ResponseEntity<PublicAdLocation> updateAdLocation(@PathVariable Integer id, @RequestBody @Valid PublicAdLocation publicAdLocation) {
        if (service.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        publicAdLocation.setId(id);
        AdLocation adLocation = mapper.publicToDomain(publicAdLocation, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.update(adLocation, id)));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete adLocation by id.", description = "Deleting an adLocation will disable all AdSpaces in the adLocation.")
    public void deleteAdLocation(@PathVariable Integer id) {
        service.deleteById(id);
    }

}
