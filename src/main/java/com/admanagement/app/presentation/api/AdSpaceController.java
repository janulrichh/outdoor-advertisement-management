package com.admanagement.app.presentation.api;

import com.admanagement.app.persistence.model.AdSpace;
import com.admanagement.app.presentation.dtos.PublicAdSpace;
import com.admanagement.app.presentation.mappers.AdSpaceMapper;
import com.admanagement.app.service.services.AdSpaceService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/adspaces")
@SecurityRequirement(name = "Bearer Authentication")
@Tag(name = "AdSpaces", description = "AdSpace Management")
public class AdSpaceController {

    private final AdSpaceService service;
    private final AdSpaceMapper mapper;
    private final EntityManager em;

    public AdSpaceController(AdSpaceService service, AdSpaceMapper mapper, EntityManager em) {
        this.service = service;
        this.mapper = mapper;
        this.em = em;
    }

    @GetMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Get adSpaces that match search parameters. (Get all adSpaces)",
            description = "Get all adSpaces when no parameters provided. Does not include adLocations that are disabled. Provide timestamps as strings in ISO 8601")
    public ResponseEntity<List<PublicAdSpace>> getAdSpaces(@Parameter(name = "Search area center point latitude") @RequestParam(required = false) Double latitude,
                                                          @Parameter(name = "Search area center point longitude") @RequestParam(required = false) Double longitude,
                                                          @Parameter(name = "Search area radius in meters") @RequestParam(required = false) Double radius,
                                                          @Parameter(name = "Search time range start timestamp") @RequestParam(required = false) String fromTime,
                                                          @Parameter(name = "Search time range end timestamp") @RequestParam(required = false) String toTime) {
        List<AdSpace> matchingAdSpaces = service.findMatching(latitude, longitude, radius, fromTime, toTime);
        List<PublicAdSpace> publicMatchingAdSpaces = mapper.domainToPublicList(matchingAdSpaces);
        return ResponseEntity.ok(publicMatchingAdSpaces);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('SALES') || hasRole('INSTALL')")
    @Operation(summary = "Get adSpace by id.")
    public ResponseEntity<PublicAdSpace> getAdSpace(@PathVariable Integer id) {
        Optional<AdSpace> adSpace = service.findById(id);
        if (adSpace.isEmpty())
            return ResponseEntity.notFound().build();
        PublicAdSpace publicAdSpace = mapper.domainToPublic(adSpace.get());
        return ResponseEntity.ok(publicAdSpace);
    }

    @GetMapping(value = "/{id}/image", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @PreAuthorize("hasRole('SALES') || hasRole('INSTALL')")
    @Operation(summary = "Get image of adSpace by id.")
    public ResponseEntity<?> getAdSpaceImage(@PathVariable Integer id,
                                             @RequestParam(required = false) Integer maxWidth,
                                             @RequestParam(required = false) Integer maxHeight) {
        Optional<AdSpace> adSpace = service.findById(id);
        if (adSpace.isEmpty())
            return ResponseEntity.notFound().build();
        try {
            byte[] imageBytes = service.getResizedImage(id, maxWidth, maxHeight);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM).body(imageBytes);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PostMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Add a new adSpace.")
    public ResponseEntity<PublicAdSpace> addAdSpace(@RequestBody @Valid PublicAdSpace publicAdSpace) {
        AdSpace adSpace = mapper.publicToDomain(publicAdSpace, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.save(adSpace)));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Update adSpace by id.")
    public ResponseEntity<PublicAdSpace> updateAdSpace(@PathVariable Integer id, @RequestBody @Valid PublicAdSpace publicAdSpace) {
        if (service.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        publicAdSpace.setId(id);
        AdSpace adSpace = mapper.publicToDomain(publicAdSpace, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.update(adSpace, id)));
    }

    @PostMapping("/{id}/image")
    @PreAuthorize("hasRole('SALES') || hasRole('INSTALL')")
    @Operation(summary = "Update image of adSpace by id.")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = @Content(mediaType = "multipart/form-data", schema = @Schema(type = "string", format = "binary")))
    public ResponseEntity<?> updateAdSpaceImage(@RequestParam("file") MultipartFile file, @PathVariable Integer id) {
        if (service.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        try {
            service.updateImage(id, file);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete adSpace by id.")
    public void deleteAdSpace(@PathVariable Integer id) {
        service.deleteById(id);
    }

}
