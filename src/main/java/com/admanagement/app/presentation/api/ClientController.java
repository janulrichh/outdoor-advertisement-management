package com.admanagement.app.presentation.api;

import com.admanagement.app.persistence.model.Client;
import com.admanagement.app.presentation.api.utils.AuthenticationUserResolver;
import com.admanagement.app.presentation.dtos.PublicClient;
import com.admanagement.app.presentation.mappers.ClientMapper;
import com.admanagement.app.service.services.ClientService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/clients")
@SecurityRequirement(name = "Bearer Authentication")
@Tag(name = "Clients", description = "Client Profile Management")
public class ClientController {

    private final AuthenticationUserResolver authenticationUserResolver;
    private final ClientService service;
    private final ClientMapper mapper;
    private final EntityManager em;

    public ClientController(ClientService service, AuthenticationUserResolver authenticationUserResolver,
                            ClientMapper mapper, EntityManager em) {
        this.service = service;
        this.authenticationUserResolver = authenticationUserResolver;
        this.mapper = mapper;
        this.em = em;
    }

    @GetMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Get all client profiles.", description = "Does not include client profiles that are disabled.")
    public ResponseEntity<List<PublicClient>> getClients() {
        List<Client> clients = service.findAll();
        List<PublicClient> publicClients = mapper.domainToPublicList(clients);
        return ResponseEntity.ok().body(publicClients);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Get client profile by id.")
    public ResponseEntity<PublicClient> getClient(@PathVariable Integer id) {
        Optional<Client> client = service.findById(id);
        if (client.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        PublicClient publicClient = mapper.domainToPublic(client.get());
        return ResponseEntity.ok(publicClient);
    }

    @PostMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Add a new client profile.")
    public ResponseEntity<PublicClient> addClient(@RequestBody @Valid PublicClient publicClient, Authentication auth) {
        publicClient.setCreatorUserId(authenticationUserResolver.getUserByAuth(auth).getId());
        Client client = mapper.publicToDomain(publicClient, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.save(client)));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Update client profile by id.")
    public ResponseEntity<PublicClient> updateClient(@PathVariable Integer id, @RequestBody @Valid PublicClient publicClient, Authentication auth) {
        if (service.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        publicClient.setCreatorUserId(authenticationUserResolver.getUserByAuth(auth).getId());
        publicClient.setId(id);
        Client client = mapper.publicToDomain(publicClient, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.update(client, id)));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete client profile by id.")
    public void deleteClient(@PathVariable Integer id) {
        service.deleteById(id);
    }
}
