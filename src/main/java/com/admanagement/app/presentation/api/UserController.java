package com.admanagement.app.presentation.api;

import com.admanagement.app.common.security.Role;
import com.admanagement.app.persistence.model.User;
import com.admanagement.app.presentation.dtos.PublicUser;
import com.admanagement.app.presentation.mappers.UserMapper;
import com.admanagement.app.service.services.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/users")
@SecurityRequirement(name = "Bearer Authentication")
@Tag(name = "Users", description = "User Account Management")
public class UserController {

    private final UserService service;
    private final UserMapper mapper;
    private final EntityManager em;

    public UserController(UserService service, UserMapper mapper, EntityManager em) {
        this.service = service;
        this.mapper = mapper;
        this.em = em;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    @Operation(summary = "Get all user accounts.", description = "Does not include user accounts that are disabled.")
    public ResponseEntity<List<PublicUser>> getUsers() {
        List<User> users = service.findAll();
        List<PublicUser> publicUsers = mapper.domainToPublicList(users);
        return ResponseEntity.ok().body(publicUsers);
    }

    @GetMapping({"/{id}", "/"})
    @PreAuthorize("hasRole('ADMIN') || hasRole('SALES') || hasRole('INSTALL')")
    @Operation(summary = "Get user account by id. (Get own user account)",
            description = "Get own user account when not providing id.")
    public ResponseEntity<PublicUser> getUser(@PathVariable(required = false) Integer id, Authentication auth) {
        Optional<User> user;
        if (id == null) {
            // Auth's own account
            user = service.findByUsername(auth.getName());
        } else {
            // Selected account
            user = service.findById(id);
        }
        if (user.isEmpty()) {
            return ResponseEntity.notFound().build();
        }
        if (!authHasAccessToTargetUser(auth, user.get())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        PublicUser publicUser = mapper.domainToPublic(user.get());
        return ResponseEntity.ok(publicUser);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    @Operation(summary = "Add a new user account.")
    public ResponseEntity<PublicUser> addUser(@RequestBody @Valid PublicUser publicUser) {
        User user = mapper.publicToDomain(publicUser, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.save(user)));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN') || hasRole('SALES') || hasRole('INSTALL')")
    @Operation(summary = "Update user account by id.")
    public ResponseEntity<PublicUser> updateUser(@PathVariable Integer id, @RequestBody @Valid PublicUser publicUser, Authentication auth) {
        Optional<User> userInRepo = service.findById(id);
        if (userInRepo.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        if (!authHasAccessToTargetUser(auth, userInRepo.get())) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
        publicUser.setId(id);
        User user = mapper.publicToDomain(publicUser, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.update(user, id)));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete user account by id.")
    public ResponseEntity<?> deleteUser(@PathVariable Integer id) {
        if (!service.canBeDeleted(id))
            return ResponseEntity.badRequest().build();
        service.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    private Boolean authHasAccessToTargetUser(Authentication auth, User targetUser) {
        String authUserName = auth.getName();
        boolean authIsAdmin = Role.hasAuthority(auth, Role.ROLE_ADMIN);
        boolean authIsSameAsTargetUserId = authUserName.equals(targetUser.getUsername());
        return authIsAdmin || authIsSameAsTargetUserId;
    }

}
