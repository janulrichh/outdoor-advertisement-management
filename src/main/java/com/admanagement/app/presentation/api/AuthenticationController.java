package com.admanagement.app.presentation.api;

import com.admanagement.app.common.security.LoginCredentials;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/")
@Tag(name = "Authentication (JWT)")
// This class does not provide any implementations but is for Swagger docs auth endpoints
public class AuthenticationController {

    @PostMapping("login")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Access granted, JWT generated."),
            @ApiResponse(responseCode = "401", description = "Error in login data.")
    })
    public void login(@RequestBody LoginCredentials data) {}  // implementation by Spring Security

    @GetMapping("logout")
    @SecurityRequirement(name = "Bearer Authentication")
    public void logout() {}  // implementation by Spring Security
}
