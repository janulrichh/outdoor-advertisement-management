package com.admanagement.app.presentation.api;

import com.admanagement.app.persistence.model.AdDesign;
import com.admanagement.app.persistence.model.Reservation;
import com.admanagement.app.presentation.api.utils.AuthenticationUserResolver;
import com.admanagement.app.presentation.dtos.PublicReservation;
import com.admanagement.app.presentation.mappers.ReservationMapper;
import com.admanagement.app.service.services.ReservationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.persistence.EntityManager;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/reservations")
@SecurityRequirement(name = "Bearer Authentication")
@Tag(name = "Reservations", description = "Reservation Management")
public class ReservationController {

    private final AuthenticationUserResolver authenticationUserResolver;
    private final ReservationService service;
    private final ReservationMapper mapper;
    private final EntityManager em;

    public ReservationController(AuthenticationUserResolver authenticationUserResolver, ReservationService service, ReservationMapper mapper, EntityManager em) {
        this.authenticationUserResolver = authenticationUserResolver;
        this.service = service;
        this.mapper = mapper;
        this.em = em;
    }

    @GetMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Get reservations that match search parameters. (Get all reservations)",
            description = "Get all reservations when no parameters provided. Provide timestamps as strings in ISO 8601")
    public ResponseEntity<List<PublicReservation>> getReservation(@Parameter(name = "Search area center point latitude") @RequestParam(required = false) Double latitude,
                                                                  @Parameter(name = "Search area center point longitude") @RequestParam(required = false) Double longitude,
                                                                  @Parameter(name = "Search area radius in meters") @RequestParam(required = false) Double radius,
                                                                  @Parameter(name = "Search time range start timestamp") @RequestParam(required = false) String fromTime,
                                                                  @Parameter(name = "Search time range end timestamp") @RequestParam(required = false) String toTime) {
        List<Reservation> matchingReservations = service.findMatching(latitude, longitude, radius, fromTime, toTime);
        List<PublicReservation> publicMatchingReservations = mapper.domainToPublicList(matchingReservations);
        return ResponseEntity.ok(publicMatchingReservations);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('SALES') || hasRole('INSTALL')")
    @Operation(summary = "Get reservation by id.")
    public ResponseEntity<PublicReservation> getReservation(@PathVariable Integer id) {
        Optional<Reservation> reservation = service.findById(id);
        if (reservation.isEmpty())
            return ResponseEntity.notFound().build();
        PublicReservation publicReservation = mapper.domainToPublic(reservation.get());
        return ResponseEntity.ok(publicReservation);
    }

    @GetMapping(value = "/{id}/image/{imageId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @PreAuthorize("hasRole('SALES') || hasRole('INSTALL')")
    @Operation(summary = "Get image of adSpace by id.")
    public ResponseEntity<?> getReservationImage(@PathVariable Integer id, @PathVariable Integer imageId,
                                                 @RequestParam(required = false) Integer maxWidth,
                                                 @RequestParam(required = false) Integer maxHeight) {
        Optional<AdDesign> adDesign = service.findAdDesignById(id, imageId);
        System.out.println("Controller teab sellist addesigin id: " + adDesign.isPresent());
        if (adDesign.isEmpty() || !adDesign.get().getImageFileExists())
            return ResponseEntity.notFound().build();
        try {
            byte[] imageBytes = service.getReservationAdDesignResizedImage(adDesign.get(), maxWidth, maxHeight);
            return ResponseEntity.ok().contentType(MediaType.APPLICATION_OCTET_STREAM).body(imageBytes);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    @PostMapping
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Add a new reservation.")
    public ResponseEntity<PublicReservation> addReservation(@RequestBody @Valid PublicReservation publicReservation,
                                                            Authentication auth) {
        Integer creatorUserId = authenticationUserResolver.getUserByAuth(auth).getId();
        publicReservation.setCreatorUserId(creatorUserId);
        publicReservation.fillIdsInConnectedEntities(creatorUserId);
        Reservation reservation = mapper.publicToDomain(publicReservation, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.save(reservation)));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Update reservation by id.")
    public ResponseEntity<PublicReservation> updateReservation(@PathVariable Integer id, @RequestBody @Valid PublicReservation publicReservation, Authentication auth) {
        if (service.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        Integer creatorUserId = authenticationUserResolver.getUserByAuth(auth).getId();
        publicReservation.setId(id);
        publicReservation.setCreatorUserId(creatorUserId);
        publicReservation.fillIdsInConnectedEntities(creatorUserId);
        Reservation reservation = mapper.publicToDomain(publicReservation, em);
        return ResponseEntity.ok(mapper.domainToPublic(service.update(reservation, id)));
    }

    @PostMapping("/{id}/image")
    @PreAuthorize("hasRole('SALES')")
    @Operation(summary = "Add new adDesign to a reservation by id.")
    @io.swagger.v3.oas.annotations.parameters.RequestBody(
            content = @Content(mediaType = "multipart/form-data", schema = @Schema(type = "string", format = "binary")))
    public ResponseEntity<?> uploadReservationImage(@RequestParam("file") MultipartFile file, @PathVariable Integer id) {
        if (service.findById(id).isEmpty())
            return ResponseEntity.notFound().build();
        try {
            service.addAdDesignImageToReservation(id, file);
        } catch (IOException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('SALES')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    @Operation(summary = "Delete reservation by id.")
    public void deleteReservation(@PathVariable Integer id) {
        service.deleteById(id);
    }
}