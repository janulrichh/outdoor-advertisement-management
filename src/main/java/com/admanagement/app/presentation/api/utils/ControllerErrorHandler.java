package com.admanagement.app.presentation.api.utils;

import com.admanagement.app.presentation.dtos.errors.ErrorResponse;
import com.admanagement.app.presentation.dtos.errors.ValidationError;
import org.hibernate.TypeMismatchException;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

@RestControllerAdvice
public class ControllerErrorHandler {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleParameterTypeMismatch(TypeMismatchException exception) {
        List<String> errors = new ArrayList<>();
        errors.add(exception.getMessage());

        ErrorResponse responseBody = new ErrorResponse();
        responseBody.setParameterErrors(errors);

        return responseBody;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleConstraintViolationError(ConstraintViolationException exception) {
        List<String> errors = new ArrayList<>();
        errors.add(exception.getConstraintName());

        ErrorResponse responseBody = new ErrorResponse();
        responseBody.setConstraintViolationErrors(errors);

        return responseBody;
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleArgumentValidationError(MethodArgumentNotValidException exception) {

        List<ValidationError> errors = new ArrayList<>();

        List<FieldError> fieldErrors = exception.getBindingResult().getFieldErrors();

        for (FieldError fieldError : fieldErrors) {
            List<String> args = Stream.of(Objects.requireNonNull(fieldError.getArguments()))
                    .filter(arg -> !(arg instanceof DefaultMessageSourceResolvable))
                    .map(String::valueOf)
                    .toList();
            ValidationError validationError = new ValidationError(fieldError.getField(),
                    fieldError.getCode(), args, fieldError.getDefaultMessage());
            errors.add(validationError);
        }

        ErrorResponse responseBody = new ErrorResponse();
        responseBody.setValidationErrors(errors);

        return responseBody;
    }
}
