package com.admanagement.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableWebMvc
@EnableScheduling
@PropertySource("classpath:/application.properties")
public class AdvertisementManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdvertisementManagerApplication.class, args);
    }

}
