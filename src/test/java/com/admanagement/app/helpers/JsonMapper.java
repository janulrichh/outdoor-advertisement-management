package com.admanagement.app.helpers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;

public class JsonMapper<T> {

    private final TypeReference<T> typeReference;
    private final TypeReference<List<T>> listTypeReference;
    private final ObjectMapper jsonMapper = new ObjectMapper();

    public JsonMapper(TypeReference<T> typeReference, TypeReference<List<T>> listTypeReference) {
        this.typeReference = typeReference;
        this.listTypeReference = listTypeReference;
    }

    public T parse(String json) throws JsonProcessingException {
        return jsonMapper.readValue(json, typeReference);
    }

    public List<T> parseList(String json) throws JsonProcessingException {
        return jsonMapper.readValue(json, listTypeReference);
    }

}
