package com.admanagement.app.helpers;

import com.admanagement.app.persistence.model.*;

import java.security.SecureRandom;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Random;

import static com.admanagement.app.helpers.TestConstants.GEOSPATIAL_LOCATION_TALLINN;
import static com.admanagement.app.helpers.TestConstants.VALID_EMAIL_ADDRESS;

public class TestDataGenerator {

    public static String getTimestamp(Integer daysDiffFromCurrentTime) {
        Instant instant = Instant.now().plus(daysDiffFromCurrentTime, ChronoUnit.DAYS);
        ZonedDateTime zonedDateTime = instant.atZone(ZoneOffset.UTC);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");  // ISO-8601
        return formatter.format(zonedDateTime);
    }

    public static Instant getInstant(Integer daysDiffFromCurrentTime) {
        return Instant.now().plus(daysDiffFromCurrentTime, ChronoUnit.DAYS);
    }

    public static String getRandomString(Integer lenghtInBytes) {
        byte[] bytes = new byte[lenghtInBytes];
        new SecureRandom().nextBytes(bytes);
        return Base64.getUrlEncoder().encodeToString(bytes);
    }

    public static byte[] getRandomBytes(Integer lengthInBytes) {
        byte[] bytes = new byte[lengthInBytes];
        new SecureRandom().nextBytes(bytes);
        return bytes;
    }

    public static Integer getRandomInteger() {
        Random random = new Random();
        return random.nextInt();
    }

    public static AdLocation getSampleAdLocation() {
        AdLocation adLocation = new AdLocation();
        adLocation.setEnabled(true);
        adLocation.setStopName(getRandomString(10));
        adLocation.setNumber(getRandomString(5));
        adLocation.setDirection('A');
        adLocation.setStreetName(getRandomString(10));
        adLocation.setComment(getRandomString(10));
        adLocation.setPosition(GEOSPATIAL_LOCATION_TALLINN);
        return adLocation;
    }

    public static AdSpace getSampleAdSpace() {
        AdSpace adSpace = new AdSpace();
        adSpace.setLocator(getRandomString(5));
        adSpace.setEnabled(true);
        adSpace.setAdLocation(getSampleAdLocation());
        return adSpace;
    }

    public static Client getSampleClient(User createdBy) {
        Client client = new Client();
        client.setCreatorUser(createdBy);
        client.setName(getRandomString(5));
        client.setEmail(VALID_EMAIL_ADDRESS);
        client.setComment(getRandomString(10));
        client.setContactPersonName(getRandomString(10));
        client.setContactPhone(getRandomString(5));
        return client;
    }

    public static Reservation getSampleReservation(User createdBy, Client forClient) {
        Reservation reservation = new Reservation();
        reservation.setCreatorUser(createdBy);
        reservation.setClient(forClient);
        reservation.setCancelled(false);
        reservation.setCampaignName(getRandomString(10));
        reservation.setInvoiceNumber(getRandomString(5));
        reservation.setComment(getRandomString(10));
        return reservation;
    }

    public static AssetGroup getSampleAssetGroup() {
        AssetGroup assetGroup = new AssetGroup();
        assetGroup.setName(getRandomString(10));
        assetGroup.setDescription(getRandomString(10));
        assetGroup.setAssetGroupType(AssetGroupType.COMBINED);
        return assetGroup;
    }

    public static User getSampleUser(UserRole role) {
        User user = new User();
        user.setEnabled(true);
        user.setUsername(getRandomString(10));
        user.setFirstName(getRandomString(10));
        user.setLastName(getRandomString(10));
        user.setPassword(getRandomString(10));
        user.setUserRole(role);
        return user;
    }
}
