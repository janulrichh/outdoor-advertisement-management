package com.admanagement.app.helpers;

import com.admanagement.app.persistence.model.*;
import com.admanagement.app.persistence.repositories.AdDesignOrderRepository;
import com.admanagement.app.service.services.AdSpaceService;
import com.admanagement.app.service.services.ClientService;
import com.admanagement.app.service.services.ReservationService;
import com.admanagement.app.service.services.UserService;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import org.locationtech.jts.geom.Point;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.Instant;

import static com.admanagement.app.helpers.TestDataGenerator.*;

@Transactional
public class TestDataSetup {

    private final ApplicationContext ctx;

    public TestDataSetup(ApplicationContext ctx) {
        this.ctx = ctx;
    }

    public User insertUser() {
        UserRole role = ctx.getBean(EntityManager.class).find(UserRole.class, 1);
        User user = getSampleUser(role);
        return ctx.getBean(UserService.class).save(user);
    }
    
    public Client insertClient(User createdBy) {
        Client client = getSampleClient(createdBy);
        return ctx.getBean(ClientService.class).save(client);
    }

    public Reservation insertReservationWithAdDesign(User createdBy, byte[] imageBytes) throws IOException {
        Client client = getSampleClient(createdBy);
        ctx.getBean(ClientService.class).save(client);

        MultipartFile image = new MockMultipartFile("image.png", imageBytes);
        Reservation reservation = getSampleReservation(createdBy, client);
        ctx.getBean(ReservationService.class).save(reservation);
        ctx.getBean(ReservationService.class).addAdDesignImageToReservation(reservation.getId(), image);
        return reservation;
    }

    public Reservation insertReservationWithReservationAdSpaceAndAdDesign(byte[] imageBytes,
                                                                          Point position,
                                                                          Instant reservationFromTime,
                                                                          Instant reservationToTime,
                                                                          User createdBy) throws IOException {
        Reservation reservation = insertReservationWithReservationAdSpace(position, reservationFromTime, reservationToTime, createdBy);

        MultipartFile image = new MockMultipartFile("image.png", imageBytes);
        ctx.getBean(ReservationService.class).addAdDesignImageToReservation(reservation.getId(), image);
        AdDesign addedAdDesign = reservation.getAdDesigns().getLast();
        reservation.getReservationAdSpaces().getLast().setAdDesign(addedAdDesign);
        addedAdDesign.getReservationAdSpaces().add(reservation.getReservationAdSpaces().getLast());
        return reservation;
    }

    public Reservation insertReservationWithReservationAdSpaceAndAdDesignOrdered(byte[] imageBytes,
                                                                                 Point position,
                                                                                 Instant reservationFromTime,
                                                                                 Instant reservationToTime,
                                                                                 User createdBy) throws IOException {
        Reservation reservation = insertReservationWithReservationAdSpaceAndAdDesign(imageBytes, position, reservationFromTime, reservationToTime, createdBy);

        AdDesign adDesign = reservation.getAdDesigns().getLast();
        AdDesignOrder adDesignOrder = new AdDesignOrder(createdBy, adDesign, Instant.now(), adDesign.getReservationAdSpaces().size());
        adDesign.getAdDesignOrders().add(adDesignOrder);
        ctx.getBean(AdDesignOrderRepository.class).save(adDesignOrder);

        return reservation;
    }

    public Reservation insertReservation(User createdBy) {
        Client client = getSampleClient(createdBy);
        ctx.getBean(ClientService.class).save(client);

        Reservation reservation = getSampleReservation(createdBy, client);

        return ctx.getBean(ReservationService.class).save(reservation);
    }

    public Reservation insertReservationWithReservationAdSpace(Point position,
                                                               Instant reservationFromTime,
                                                               Instant reservationToTime,
                                                               User createdBy) {
        AdSpace adSpace = getSampleAdSpace();
        adSpace.getAdLocation().setPosition(position);
        ctx.getBean(AdSpaceService.class).save(adSpace);

        return insertReservationWithReservationAdSpace(adSpace, reservationFromTime, reservationToTime, createdBy);
    }

    public Reservation insertReservationWithReservationAdSpace(AdSpace adSpace,
                                                               Instant reservationFromTime,
                                                               Instant reservationToTime,
                                                               User createdBy) {
        Client client = getSampleClient(createdBy);
        ctx.getBean(ClientService.class).save(client);

        Reservation reservation = getSampleReservation(createdBy, client);

        ReservationAdSpace reservationAdSpace = new ReservationAdSpace();
        reservationAdSpace.setFromTime(reservationFromTime);
        reservationAdSpace.setToTime(reservationToTime);
        reservationAdSpace.setInstallerUser(createdBy);
        reservationAdSpace.setAdSpace(adSpace);
        reservation.getReservationAdSpaces().add(reservationAdSpace);

        return ctx.getBean(ReservationService.class).save(reservation);
    }

}
