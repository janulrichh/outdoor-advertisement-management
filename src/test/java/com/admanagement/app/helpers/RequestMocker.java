package com.admanagement.app.helpers;

import com.admanagement.app.presentation.dtos.PublicBaseEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.MultiValueMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

public class RequestMocker {

    private final String controllerMapping;
    private final ObjectMapper jsonMapper = new ObjectMapper();
    private final MockMvc mockMvc;

    public RequestMocker(String controllerMapping, MockMvc mockMvc) {
        this.controllerMapping = controllerMapping;
        this.mockMvc = mockMvc;
    }

    public MockHttpServletResponse getFrom(String uri, HttpHeaders headers) throws Exception {
        return mockMvc.perform(get(controllerMapping + "/" + uri).headers(headers)).andReturn().getResponse();
    }

    public MockHttpServletResponse getFrom(String uri) throws Exception {
        return mockMvc.perform(get(controllerMapping + "/" + uri)).andReturn().getResponse();
    }

    public MockHttpServletResponse getEntity(Integer id) throws Exception {
        return mockMvc.perform(get(controllerMapping + "/" + id)).andReturn().getResponse();
    }

    public MockHttpServletResponse getEntities() throws Exception {
        return mockMvc.perform(get(controllerMapping)).andReturn().getResponse();
    }

    public MockHttpServletResponse getEntities(MultiValueMap<String, String> params) throws Exception {
        return mockMvc.perform(get(controllerMapping).params(params)).andReturn().getResponse();
    }

    public MockHttpServletResponse postTo(Object body, String uri) throws Exception {
        String bodyString = jsonMapper.writeValueAsString(body);
        return mockMvc
                .perform(post(controllerMapping + uri).content(bodyString).contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
    }

    public MockHttpServletResponse postMultipartTo(MockMultipartFile body, String uri) throws Exception {
        return mockMvc
                .perform(multipart(controllerMapping + uri).file(body)).andReturn().getResponse();
    }

    public <T extends PublicBaseEntity> MockHttpServletResponse postEntity(T entity) throws Exception {
        String body = jsonMapper.writeValueAsString(entity);
        return mockMvc
                .perform(post(controllerMapping).content(body).contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
    }

    public MockHttpServletResponse putTo(Object body, String uri) throws Exception {
        String bodyString = jsonMapper.writeValueAsString(body);
        return mockMvc
                .perform(put(controllerMapping + uri).content(bodyString).contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
    }

    public <T extends PublicBaseEntity> MockHttpServletResponse putEntity(Integer id, T entity) throws Exception {
        String body = jsonMapper.writeValueAsString(entity);
        return mockMvc
                .perform(put(controllerMapping + "/" + id).content(body).contentType(MediaType.APPLICATION_JSON))
                .andReturn().getResponse();
    }

    public MockHttpServletResponse deleteEntity(Integer id) throws Exception {
        return mockMvc.perform(delete(controllerMapping + "/" + id)).andReturn().getResponse();
    }
}
