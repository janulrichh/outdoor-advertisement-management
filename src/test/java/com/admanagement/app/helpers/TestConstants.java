package com.admanagement.app.helpers;

import com.admanagement.app.service.utils.GeometryUtility;
import org.locationtech.jts.geom.Point;

public class TestConstants {
    public static final String INSTALLATION_CONTROLLER_MAPPING = "/api/install";
    public static final String USER_CONTROLLER_MAPPING = "/api/users";
    public static final String CLIENT_CONTROLLER_MAPPING = "/api/clients";
    public static final String ADLOCATION_CONTROLLER_MAPPING = "/api/adlocations";
    public static final String ADSPACE_CONTROLLER_MAPPING = "/api/adspaces";
    public static final String ASSETGROUP_CONTROLLER_MAPPING = "/api/assetgroups";
    public static final String RESERVATION_CONTROLLER_MAPPING = "/api/reservations";
    public static final String PRINT_CONTROLLER_MAPPING = "/api/print";

    public static final String API_MAPPING = "/api";


    public static final String ILLEGAL_EMAIL_ADDRESS = "%3?E/AgT#1€";
    public static final String VALID_EMAIL_ADDRESS = "name@domain.com";

    public static final Point GEOSPATIAL_LOCATION_TALLINN = GeometryUtility.toPoint(59.43703300279, 24.75355239736);
    public static final Point GEOSPATIAL_LOCATION_HELSINKI = GeometryUtility.toPoint(60.1695193361, 24.93900874516);

}
