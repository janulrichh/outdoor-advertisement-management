package com.admanagement.app;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.event.annotation.BeforeTestExecution;

@SpringBootTest
@ActiveProfiles("test")
class AdvertisementManagerApplicationTests {

    @Autowired
    private Environment env;


    @BeforeTestExecution
    private void cleanTestDatabase() {
        Flyway flyway = Flyway.configure()
                .dataSource(env.getProperty("postgres.url"),
                        env.getProperty("postgres.user"),
                        env.getProperty("postgres.pass"))
                .cleanDisabled(false)
                .load();
        flyway.clean();
    }
}