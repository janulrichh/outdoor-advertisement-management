package com.admanagement.app.presentation.api;

import com.admanagement.app.common.ConfigurationReader;
import com.admanagement.app.helpers.JsonMapper;
import com.admanagement.app.helpers.RequestMocker;
import com.admanagement.app.helpers.TestDataSetup;
import com.admanagement.app.persistence.model.Reservation;
import com.admanagement.app.persistence.repositories.AdDesignOrderRepository;
import com.admanagement.app.presentation.dtos.PublicAdDesign;
import com.admanagement.app.presentation.dtos.PublicAdDesignPrintOrderRequest;
import com.admanagement.app.presentation.dtos.PublicPrintServiceAddress;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.Assume;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.core.env.Environment;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import static com.admanagement.app.helpers.TestConstants.*;
import static com.admanagement.app.helpers.TestDataGenerator.*;
import static org.junit.jupiter.api.Assertions.*;

public class PrintControllerIntegrationTest extends BaseEntityControllerIntegrationTest<PublicAdDesign> {

    private Environment env;

    @BeforeAll
    protected void setup() {
        jsonMapper = new JsonMapper<>(new TypeReference<PublicAdDesign>() {}, new TypeReference<List<PublicAdDesign>>() {});
        requestMocker = new RequestMocker(PRINT_CONTROLLER_MAPPING, mockMvc);
        testDataSetup = new TestDataSetup(ctx);
        env = ctx.getBean(Environment.class);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetUnOrderedAdDesigns() throws Exception {
        Reservation reservation = testDataSetup.insertReservationWithReservationAdSpaceAndAdDesign(getRandomBytes(1000),
                GEOSPATIAL_LOCATION_TALLINN, getInstant(0), getInstant(+10), getActiveUser());

        var response = requestMocker.getEntities();
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities.stream().anyMatch(e -> e.getReservationId().equals(reservation.getId())));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetUnOrderedAdDesignsWhenAlreadyOrdered() throws Exception {
        Reservation reservation = testDataSetup.insertReservationWithReservationAdSpaceAndAdDesignOrdered(getRandomBytes(1000),
                GEOSPATIAL_LOCATION_TALLINN, getInstant(0), getInstant(+10), getActiveUser());

        var response = requestMocker.getEntities();
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertFalse(responseEntities.stream().anyMatch(e -> e.getReservationId().equals(reservation.getId())));
    }

    @Test
    public void testGetOrderedImageByPublicId() throws Exception {
        byte[] imageBytes = getRandomBytes(1000);
        Reservation reservation = testDataSetup.insertReservationWithAdDesign(testDataSetup.insertUser(), imageBytes);
        String publicId = getRandomString(10);
        reservation.getAdDesigns().getLast().setPublicId(publicId);

        var response = requestMocker.getFrom("/order/" + publicId);

        assertEquals(200, response.getStatus());
        assertArrayEquals(imageBytes, response.getContentAsByteArray());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testSendAdDesignsToPrint() throws Exception {
        // Run test only if STMP configuration is present
        Assume.assumeFalse(Objects.equals(env.getProperty("spring.mail.host"), ""));

        Reservation reservation = testDataSetup.insertReservationWithAdDesign(getActiveUser(), getRandomBytes(1000));
        var body = new PublicAdDesignPrintOrderRequest();
        Integer adDesignId = reservation.getAdDesigns().getLast().getId();
        Integer amount = getRandomInteger();
        body.setAdDesignAmounts(new HashMap<>() {{
            put(adDesignId, amount);
        }});

        var response = requestMocker.postTo(body, "/send");

        assertEquals(200, response.getStatus());
        assertTrue(ctx.getBean(AdDesignOrderRepository.class).findAll().stream().anyMatch(
                e -> e.getAdDesign().getId().equals(adDesignId) && e.getOrderAmount().equals(amount)));
        assertNotNull(reservation.getAdDesigns().getLast().getPublicId());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
    public void testUpdateAddress() throws Exception {
        var body = new PublicPrintServiceAddress(VALID_EMAIL_ADDRESS);

        var response = requestMocker.putTo(body, "/address");

        assertEquals(200, response.getStatus());
        assertEquals(VALID_EMAIL_ADDRESS, ctx.getBean(ConfigurationReader.class).getConfigurationValue("ORDER_RECIPIENT"));
    }

    @Test
    @WithMockUser(username = "user")
    public void testAccessControl() throws Exception {
        var response = requestMocker.getEntities();

        assertEquals(401, response.getStatus());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testValidation() throws Exception {
        var body = new PublicAdDesignPrintOrderRequest();
        body.setAdDesignAmounts(null);

        var response = requestMocker.postTo(body, "/send");

        assertEquals(400, response.getStatus());
    }
}

