package com.admanagement.app.presentation.api;

import com.admanagement.app.helpers.JsonMapper;
import com.admanagement.app.helpers.RequestMocker;
import com.admanagement.app.helpers.TestDataSetup;
import com.admanagement.app.presentation.dtos.PublicReservation;
import com.admanagement.app.presentation.mappers.ReservationMapper;
import com.admanagement.app.service.services.ReservationService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.stream.Stream;

import static com.admanagement.app.helpers.TestConstants.*;
import static com.admanagement.app.helpers.TestDataGenerator.*;
import static org.junit.jupiter.api.Assertions.*;

public class ReservationControllerIntegrationTest extends BaseEntityControllerIntegrationTest<PublicReservation> {

    @Autowired
    private ReservationService service;
    @Autowired
    private ReservationMapper mapper;

    @BeforeAll
    protected void setup() {
        jsonMapper = new JsonMapper<>(new TypeReference<PublicReservation>() {}, new TypeReference<List<PublicReservation>>() {});
        requestMocker = new RequestMocker(RESERVATION_CONTROLLER_MAPPING, mockMvc);
        testDataSetup = new TestDataSetup(ctx);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAllReservations() throws Exception {
        var entity1 = testDataSetup.insertReservation(getActiveUser());
        var entity2 = testDataSetup.insertReservation(getActiveUser());

        var response = requestMocker.getEntities();
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities
                .containsAll(Stream.of(entity1, entity2).map(e -> mapper.domainToPublic(e)).toList()));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetReservationsInArea() throws Exception {
        var entity = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(0), getInstant(+10), getActiveUser());

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("latitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getX()));
        params.add("longitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getY()));
        params.add("radius", String.valueOf(100_000));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetReservationsInAreaOutOfRadius() throws Exception {
        var entity = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(0), getInstant(+10), getActiveUser());

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("latitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getX()));
        params.add("longitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getY()));
        params.add("radius", String.valueOf(50_000));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertFalse(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetReservationsInTimeRange() throws Exception {
        var entity = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(0), getInstant(+10), getActiveUser());

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("fromTime", getTimestamp(-5));
        params.add("toTime", getTimestamp(+5));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetReservationsInTimeRangeNotInRange() throws Exception {
        var entity = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(0), getInstant(+10), getActiveUser());

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("fromTime", getTimestamp(+11));
        params.add("toTime", getTimestamp(+15));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertFalse(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetReservationById() throws Exception {
        var entity = testDataSetup.insertReservation(getActiveUser());

        var response = requestMocker.getEntity(entity.getId());
        var responseEntity = jsonMapper.parse(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertEquals(mapper.domainToPublic(entity), responseEntity);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testAddReservation() throws Exception {
        var newEntity = getSampleReservation(getActiveUser(), testDataSetup.insertClient(getActiveUser()));
        var publicNewEntity = mapper.domainToPublic(newEntity);

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(
                e -> mapper.domainToPublic(e).getCampaignName().equals(publicNewEntity.getCampaignName())
        ));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testAddReservationImage() throws Exception {
        var entity = testDataSetup.insertReservation(getActiveUser());
        byte[] imageBytes = getRandomBytes(1000);
        String imageFileName = getRandomString(10) + ".png";
        MockMultipartFile multipartFile = new MockMultipartFile("file", imageFileName, "", imageBytes);

        var response = requestMocker.postMultipartTo(multipartFile, "/" + entity.getId() + "/image");

        assertEquals(200, response.getStatus());
        assertEquals(imageBytes, service.findById(entity.getId()).orElseThrow().getAdDesigns().getLast().getImage());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void getReservationImage() throws Exception {
        byte[] imageBytes = getRandomBytes(1000);
        var entity = testDataSetup.insertReservationWithAdDesign(getActiveUser(), imageBytes);
        var newAdDesign = entity.getAdDesigns().getLast();

        var response = requestMocker.getFrom(entity.getId() + "/image/" + newAdDesign.getId());

        assertEquals(200, response.getStatus());
        assertEquals(MediaType.APPLICATION_OCTET_STREAM_VALUE, response.getContentType());
        assertArrayEquals(imageBytes, response.getContentAsByteArray());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testUpdateReservation() throws Exception {
        var entity = testDataSetup.insertReservation(getActiveUser());
        var publicEntity = mapper.domainToPublic(entity);
        publicEntity.setCampaignName(getRandomString(10));

        var response = requestMocker.putEntity(entity.getId(), publicEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(
                e -> mapper.domainToPublic(e).getCampaignName().equals(publicEntity.getCampaignName())
        ));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testDeleteReservation() throws Exception {
        var entity = testDataSetup.insertReservation(getActiveUser());

        var response = requestMocker.deleteEntity(entity.getId());

        assertEquals(204, response.getStatus());
        assertFalse(service.findAll().contains(entity));
    }

    @Test
    @WithMockUser(username = "user")
    public void testAccessControl() throws Exception {
        var response = requestMocker.getEntities();

        assertEquals(401, response.getStatus());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testValidation() throws Exception {
        var newEntity = testDataSetup.insertReservation(getActiveUser());
        var publicNewEntity = mapper.domainToPublic(newEntity);
        publicNewEntity.setCampaignName(null);

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(400, response.getStatus());
    }
}
