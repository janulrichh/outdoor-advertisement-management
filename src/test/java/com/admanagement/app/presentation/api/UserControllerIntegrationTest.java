package com.admanagement.app.presentation.api;

import com.admanagement.app.helpers.JsonMapper;
import com.admanagement.app.helpers.RequestMocker;
import com.admanagement.app.helpers.TestDataSetup;
import com.admanagement.app.presentation.dtos.PublicUser;
import com.admanagement.app.presentation.mappers.UserMapper;
import com.admanagement.app.service.services.UserService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.stream.Stream;

import static com.admanagement.app.helpers.TestConstants.USER_CONTROLLER_MAPPING;
import static com.admanagement.app.helpers.TestDataGenerator.getRandomString;
import static com.admanagement.app.helpers.TestDataGenerator.getSampleUser;
import static org.junit.jupiter.api.Assertions.*;

public class UserControllerIntegrationTest extends BaseEntityControllerIntegrationTest<PublicUser> {

    @Autowired
    private UserService service;
    @Autowired
    private UserMapper mapper;

    @BeforeAll
    protected void setup() {
        jsonMapper = new JsonMapper<>(new TypeReference<PublicUser>() {}, new TypeReference<List<PublicUser>>() {});
        requestMocker = new RequestMocker(USER_CONTROLLER_MAPPING, mockMvc);
        testDataSetup = new TestDataSetup(ctx);
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
    public void testGetAllUsers() throws Exception {
        var entity1 = testDataSetup.insertUser();
        var entity2 = testDataSetup.insertUser();

        var response = requestMocker.getEntities();
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities
                .containsAll(Stream.of(entity1, entity2).map(e -> mapper.domainToPublic(e)).toList()));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
    public void testGetUserById() throws Exception {
        var entity = testDataSetup.insertUser();

        var response = requestMocker.getEntity(entity.getId());
        var responseEntity = jsonMapper.parse(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertEquals(mapper.domainToPublic(entity), responseEntity);
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
    public void testAddUser() throws Exception {
        var newEntity = getSampleUser(null);
        var publicNewEntity = mapper.domainToPublic(newEntity);
        publicNewEntity.setUserRoleId(1);
        publicNewEntity.setPassword(getRandomString(10));

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(
                e -> mapper.domainToPublic(e).getUsername().equals(publicNewEntity.getUsername())
        ));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
    public void testUpdateUser() throws Exception {
        var entity = testDataSetup.insertUser();
        var publicEntity = mapper.domainToPublic(entity);
        publicEntity.setUsername(getRandomString(10));
        publicEntity.setPassword(getRandomString(10));

        var response = requestMocker.putEntity(entity.getId(), publicEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(
                e -> mapper.domainToPublic(e).getUsername().equals(publicEntity.getUsername())
        ));
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
    public void testDeleteUser() throws Exception {
        var entity = testDataSetup.insertUser();

        var response = requestMocker.deleteEntity(entity.getId());

        assertEquals(204, response.getStatus());
        assertFalse(service.findAll().contains(entity));
    }

    @Test
    @WithMockUser(username = "user")
    public void testAccessControl() throws Exception {
        var response = requestMocker.getEntities();

        assertEquals(401, response.getStatus());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testValidation() throws Exception {
        var newEntity = getSampleUser(null);
        var publicNewEntity = mapper.domainToPublic(newEntity);
        publicNewEntity.setUserRoleId(null);

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(400, response.getStatus());
    }
}
