package com.admanagement.app.presentation.api;

import com.admanagement.app.helpers.JsonMapper;
import com.admanagement.app.helpers.TestDataSetup;
import com.admanagement.app.persistence.model.User;
import com.admanagement.app.presentation.dtos.PublicBaseEntity;
import com.admanagement.app.service.services.UserService;
import jakarta.transaction.Transactional;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Transactional
public abstract class BaseEntityControllerIntegrationTest<TPublic extends PublicBaseEntity>
        extends BaseIntegrationTest {

    protected JsonMapper<TPublic> jsonMapper;
    protected TestDataSetup testDataSetup;

    protected User getActiveUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        return ctx.getBean(UserService.class).findByUsername(username).orElseThrow();
    }

}
