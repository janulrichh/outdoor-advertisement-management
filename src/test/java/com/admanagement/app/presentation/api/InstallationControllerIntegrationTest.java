package com.admanagement.app.presentation.api;

import com.admanagement.app.helpers.JsonMapper;
import com.admanagement.app.helpers.RequestMocker;
import com.admanagement.app.helpers.TestDataSetup;
import com.admanagement.app.persistence.model.Reservation;
import com.admanagement.app.presentation.dtos.PublicReservation;
import com.admanagement.app.service.services.ReservationService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.time.Instant;
import java.util.List;

import static com.admanagement.app.helpers.TestConstants.*;
import static com.admanagement.app.helpers.TestDataGenerator.getInstant;
import static org.junit.jupiter.api.Assertions.*;

public class InstallationControllerIntegrationTest extends BaseEntityControllerIntegrationTest<PublicReservation> {

    @Autowired
    private ReservationService service;

    @BeforeAll
    protected void setup() {
        jsonMapper = new JsonMapper<>(new TypeReference<PublicReservation>() {}, new TypeReference<List<PublicReservation>>() {});
        requestMocker = new RequestMocker(INSTALLATION_CONTROLLER_MAPPING, mockMvc);
        testDataSetup = new TestDataSetup(ctx);
    }

    @Test
    @WithMockUser(username = "install", authorities = {"ROLE_INSTALL"})
    public void testGetReservationsToInstall() throws Exception {
        Reservation reservation = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(10), getInstant(20), getActiveUser());

        var getResponse = requestMocker.getEntities();
        var entities = jsonMapper.parseList(getResponse.getContentAsString());

        assertEquals(MediaType.APPLICATION_JSON.toString(), getResponse.getContentType());
        assertEquals(200, getResponse.getStatus());
        assertTrue(entities.stream().anyMatch(e -> e.getId().equals(reservation.getId())));
    }

    @Test
    @WithMockUser(username = "install", authorities = {"ROLE_INSTALL"})
    public void testGetReservationsToInstallAlreadyInstalled() throws Exception {
        Reservation reservation = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(10), getInstant(20), getActiveUser());
        reservation.getReservationAdSpaces().getLast().setAdInstalledAt(Instant.now());

        var getResponse = requestMocker.getEntities();
        var entities = jsonMapper.parseList(getResponse.getContentAsString());

        assertEquals(MediaType.APPLICATION_JSON.toString(), getResponse.getContentType());
        assertEquals(200, getResponse.getStatus());
        assertFalse(entities.stream().anyMatch(e -> e.getId().equals(reservation.getId())));
    }

    @Test
    @WithMockUser(username = "install", authorities = {"ROLE_INSTALL"})
    public void testGetReservationsToInstallInArea() throws Exception {
        Reservation reservation = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(10), getInstant(20), getActiveUser());

        MultiValueMap<String, String> getParams = new LinkedMultiValueMap<>();
        getParams.add("latitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getX()));
        getParams.add("longitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getY()));
        getParams.add("radius", String.valueOf(100_000));
        var getResponse = requestMocker.getEntities(getParams);
        var entities = jsonMapper.parseList(getResponse.getContentAsString());

        assertEquals(MediaType.APPLICATION_JSON.toString(), getResponse.getContentType());
        assertEquals(200, getResponse.getStatus());
        assertTrue(entities.stream().anyMatch(e -> e.getId().equals(reservation.getId())));
    }

    @Test
    @WithMockUser(username = "install", authorities = {"ROLE_INSTALL"})
    public void testGetReservationsToInstallInAreaOutOfRadius() throws Exception {
        Reservation reservation = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(10), getInstant(20), getActiveUser());

        MultiValueMap<String, String> getParams = new LinkedMultiValueMap<>();
        getParams.add("latitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getX()));
        getParams.add("longitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getY()));
        getParams.add("radius", String.valueOf(50_000));
        var getResponse = requestMocker.getEntities(getParams);
        var entities = jsonMapper.parseList(getResponse.getContentAsString());

        assertEquals(MediaType.APPLICATION_JSON.toString(), getResponse.getContentType());
        assertEquals(200, getResponse.getStatus());
        assertFalse(entities.stream().anyMatch(e -> e.getId().equals(reservation.getId())));
    }

    @Test
    @WithMockUser(username = "install", authorities = {"ROLE_INSTALL"})
    public void testUpdateInstallationStatusOfReservationAdSpace() throws Exception {
        Reservation reservation = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(10), getInstant(20), getActiveUser());
        Integer rsIdToMarkAsInstalled = reservation.getReservationAdSpaces().getFirst().getId();

        var response = requestMocker.putTo(rsIdToMarkAsInstalled, "/" + rsIdToMarkAsInstalled);

        assertEquals(200, response.getStatus());
        assertFalse(service.findReservationsThatRequireAdDesignInstallation().stream().anyMatch(e -> e.getId().equals(reservation.getId())));
    }

    @Test
    @WithMockUser(username = "install", authorities = {"ROLE_INSTALL"})
    public void testReverseInstallationStatusOfReservationAdSpace() throws Exception {
        Reservation reservation = testDataSetup.insertReservationWithReservationAdSpace(GEOSPATIAL_LOCATION_TALLINN,
                getInstant(10), getInstant(20), getActiveUser());
        reservation.getReservationAdSpaces().getLast().setAdInstalledAt(Instant.now());

        var response = requestMocker.deleteEntity(reservation.getReservationAdSpaces().getLast().getId());

        assertEquals(200, response.getStatus());
        assertTrue(service.findReservationsThatRequireAdDesignInstallation().stream().anyMatch(e -> e.getId().equals(reservation.getId())));
    }

    @Test
    @WithMockUser(username = "user")
    public void testAccessControl() throws Exception {
        var getResponse = requestMocker.getEntities();

        assertEquals(401, getResponse.getStatus());
    }

}

