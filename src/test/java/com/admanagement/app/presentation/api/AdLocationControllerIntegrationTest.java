package com.admanagement.app.presentation.api;

import com.admanagement.app.helpers.JsonMapper;
import com.admanagement.app.helpers.RequestMocker;
import com.admanagement.app.presentation.dtos.PublicAdLocation;
import com.admanagement.app.presentation.mappers.AdLocationMapper;
import com.admanagement.app.service.services.AdLocationService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.stream.Stream;

import static com.admanagement.app.helpers.TestConstants.*;
import static com.admanagement.app.helpers.TestDataGenerator.getRandomString;
import static com.admanagement.app.helpers.TestDataGenerator.getSampleAdLocation;
import static org.junit.jupiter.api.Assertions.*;

public class AdLocationControllerIntegrationTest extends BaseEntityControllerIntegrationTest<PublicAdLocation> {

    @Autowired
    private AdLocationService service;
    @Autowired
    private AdLocationMapper mapper;

    @BeforeAll
    protected void setup() {
        jsonMapper = new JsonMapper<>(new TypeReference<PublicAdLocation>() {}, new TypeReference<List<PublicAdLocation>>() {});
        requestMocker = new RequestMocker(ADLOCATION_CONTROLLER_MAPPING, mockMvc);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAllAdLocations() throws Exception {
        var entity1 = getSampleAdLocation();
        var entity2 = getSampleAdLocation();
        service.save(entity1);
        service.save(entity2);

        var response = requestMocker.getEntities();
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities
                .containsAll(Stream.of(entity1, entity2).map(e -> mapper.domainToPublic(e)).toList()));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAdLocationsInArea() throws Exception {
        var entity = getSampleAdLocation();
        entity.setPosition(GEOSPATIAL_LOCATION_TALLINN);
        service.save(entity);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("latitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getX()));
        params.add("longitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getY()));
        params.add("radius", String.valueOf(100_000));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAdLocationsInAreaOutOfRadius() throws Exception {
        var entity = getSampleAdLocation();
        entity.setPosition(GEOSPATIAL_LOCATION_TALLINN);
        service.save(entity);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("latitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getX()));
        params.add("longitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getY()));
        params.add("radius", String.valueOf(50_000));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertFalse(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAdLocationById() throws Exception {
        var entity = getSampleAdLocation();
        service.save(entity);

        var response = requestMocker.getEntity(entity.getId());
        var responseEntity = jsonMapper.parse(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertEquals(mapper.domainToPublic(entity), responseEntity);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testAddAdLocation() throws Exception {
        var newEntity = getSampleAdLocation();
        var publicNewEntity = mapper.domainToPublic(newEntity);

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(e -> mapper.domainToPublic(e).equals(publicNewEntity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testUpdateAdLocation() throws Exception {
        var entity = getSampleAdLocation();
        service.save(entity);
        var publicEntity = mapper.domainToPublic(entity);
        publicEntity.setNumber(getRandomString(5));

        var response = requestMocker.putEntity(entity.getId(), publicEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(e -> mapper.domainToPublic(e).equals(publicEntity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testDeleteAdLocation() throws Exception {
        var entity = getSampleAdLocation();
        service.save(entity);

        var response = requestMocker.deleteEntity(entity.getId());

        assertEquals(204, response.getStatus());
        assertFalse(service.findAll().contains(entity));
    }

    @Test
    @WithMockUser(username = "user")
    public void testAccessControl() throws Exception {
        var response = requestMocker.getEntities();

        assertEquals(401, response.getStatus());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testValidation() throws Exception {
        var newEntity = getSampleAdLocation();
        var publicNewEntity = mapper.domainToPublic(newEntity);
        publicNewEntity.setNumber(null);

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(400, response.getStatus());
    }
}
