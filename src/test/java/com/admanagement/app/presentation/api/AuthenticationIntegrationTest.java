package com.admanagement.app.presentation.api;

import com.admanagement.app.common.security.LoginCredentials;
import com.admanagement.app.helpers.RequestMocker;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.security.test.context.support.WithMockUser;

import static com.admanagement.app.helpers.TestConstants.API_MAPPING;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class AuthenticationIntegrationTest extends BaseIntegrationTest {

    @BeforeAll
    protected void setup() {
        requestMocker = new RequestMocker(API_MAPPING, mockMvc);
    }

    @Test
    public void testLogin() throws Exception {
        var loginCredentials = new LoginCredentials("admin", "secret");

        var response = requestMocker.postTo(loginCredentials, "/login");

        assertEquals(200, response.getStatus());
        assertNotNull(response.getHeader("Authorization"));
    }

    @Test
    public void testJwtAuthorization() throws Exception {
        var loginCredentials = new LoginCredentials("admin", "secret");
        var response = requestMocker.postTo(loginCredentials, "/login");
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", response.getHeader("Authorization"));

        response = requestMocker.getFrom("/users", headers);

        assertEquals(200, response.getStatus());
    }

    @Test
    @WithMockUser(username = "admin", authorities = {"ROLE_ADMIN"})
    public void testLogout() throws Exception {
        var response = requestMocker.getFrom("/logout");

        assertEquals(200, response.getStatus());
        assertEquals(401, requestMocker.getFrom("/users").getStatus());
    }
}
