package com.admanagement.app.presentation.api;

import com.admanagement.app.helpers.JsonMapper;
import com.admanagement.app.helpers.RequestMocker;
import com.admanagement.app.persistence.model.User;
import com.admanagement.app.presentation.dtos.PublicClient;
import com.admanagement.app.presentation.mappers.ClientMapper;
import com.admanagement.app.service.services.ClientService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.stream.Stream;

import static com.admanagement.app.helpers.TestConstants.CLIENT_CONTROLLER_MAPPING;
import static com.admanagement.app.helpers.TestConstants.ILLEGAL_EMAIL_ADDRESS;
import static com.admanagement.app.helpers.TestDataGenerator.getRandomString;
import static com.admanagement.app.helpers.TestDataGenerator.getSampleClient;
import static org.junit.jupiter.api.Assertions.*;

public class ClientControllerIntegrationTest extends BaseEntityControllerIntegrationTest<PublicClient> {

    @Autowired
    private ClientService service;
    @Autowired
    private ClientMapper mapper;

    @BeforeAll
    protected void setup() {
        jsonMapper = new JsonMapper<>(new TypeReference<PublicClient>() {}, new TypeReference<List<PublicClient>>() {});
        requestMocker = new RequestMocker(CLIENT_CONTROLLER_MAPPING, mockMvc);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAllClients() throws Exception {
        var entity1 = getSampleClient(getActiveUser());
        var entity2 = getSampleClient(getActiveUser());
        service.save(entity1);
        service.save(entity2);

        var response = requestMocker.getEntities();
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities
                .containsAll(Stream.of(entity1, entity2).map(e -> mapper.domainToPublic(e)).toList()));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetClientById() throws Exception {
        var entity = getSampleClient(getActiveUser());
        service.save(entity);

        var response = requestMocker.getEntity(entity.getId());
        var responseEntity = jsonMapper.parse(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertEquals(mapper.domainToPublic(entity), responseEntity);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testAddClientAndAutomaticallyGeneratesCreator() throws Exception {
        User user = getActiveUser();
        var newEntity = getSampleClient(null);
        var publicNewEntity = mapper.domainToPublic(newEntity);

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(
                e -> mapper.domainToPublic(e).getName().equals(publicNewEntity.getName()) &&
                        mapper.domainToPublic(e).getCreatorUserId().equals(user.getId())
        ));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testUpdateClient() throws Exception {
        var entity = getSampleClient(getActiveUser());
        service.save(entity);
        var publicEntity = mapper.domainToPublic(entity);
        publicEntity.setName(getRandomString(5));

        var response = requestMocker.putEntity(entity.getId(), publicEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(e -> mapper.domainToPublic(e).equals(publicEntity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testDeleteClient() throws Exception {
        var entity = getSampleClient(getActiveUser());
        service.save(entity);

        var response = requestMocker.deleteEntity(entity.getId());

        assertEquals(204, response.getStatus());
        assertFalse(service.findAll().contains(entity));
    }

    @Test
    @WithMockUser(username = "user")
    public void testAccessControl() throws Exception {
        var response = requestMocker.getEntities();

        assertEquals(401, response.getStatus());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testValidation() throws Exception {
        var newEntity = getSampleClient(getActiveUser());
        var publicNewEntity = mapper.domainToPublic(newEntity);
        publicNewEntity.setEmail(ILLEGAL_EMAIL_ADDRESS);

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(400, response.getStatus());
    }
}
