package com.admanagement.app.presentation.api;

import com.admanagement.app.helpers.JsonMapper;
import com.admanagement.app.helpers.RequestMocker;
import com.admanagement.app.helpers.TestDataSetup;
import com.admanagement.app.presentation.dtos.PublicAdSpace;
import com.admanagement.app.presentation.mappers.AdSpaceMapper;
import com.admanagement.app.service.services.AdLocationService;
import com.admanagement.app.service.services.AdSpaceService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.stream.Stream;

import static com.admanagement.app.helpers.TestConstants.*;
import static com.admanagement.app.helpers.TestDataGenerator.*;
import static org.junit.jupiter.api.Assertions.*;

public class AdSpaceControllerIntegrationTest extends BaseEntityControllerIntegrationTest<PublicAdSpace> {

    @Autowired
    private AdSpaceService service;
    @Autowired
    private AdSpaceMapper mapper;

    @BeforeAll
    protected void setup() {
        jsonMapper = new JsonMapper<>(new TypeReference<PublicAdSpace>() {}, new TypeReference<List<PublicAdSpace>>() {});
        requestMocker = new RequestMocker(ADSPACE_CONTROLLER_MAPPING, mockMvc);
        testDataSetup = new TestDataSetup(ctx);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAllAdSpaces() throws Exception {
        var entity1 = getSampleAdSpace();
        var entity2 = getSampleAdSpace();
        service.save(entity1);
        service.save(entity2);

        var response = requestMocker.getEntities();
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities
                .containsAll(Stream.of(entity1, entity2).map(e -> mapper.domainToPublic(e)).toList()));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAdSpacesInArea() throws Exception {
        var entity = getSampleAdSpace();
        entity.getAdLocation().setPosition(GEOSPATIAL_LOCATION_TALLINN);
        service.save(entity);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("latitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getX()));
        params.add("longitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getY()));
        params.add("radius", String.valueOf(100_000));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAdSpacesInAreaOutOfRadius() throws Exception {
        var entity = getSampleAdSpace();
        entity.getAdLocation().setPosition(GEOSPATIAL_LOCATION_TALLINN);
        service.save(entity);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("latitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getX()));
        params.add("longitude", String.valueOf(GEOSPATIAL_LOCATION_HELSINKI.getY()));
        params.add("radius", String.valueOf(50_000));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertFalse(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAdSpacesInTimeRangeAvailable() throws Exception {
        var entity = getSampleAdSpace();
        service.save(entity);

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("fromTime", getTimestamp(+1));
        params.add("toTime", getTimestamp(+10));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAdSpacesInTimeRangeNotAvailable() throws Exception {
        var entity = getSampleAdSpace();
        service.save(entity);
        testDataSetup.insertReservationWithReservationAdSpace(
                entity, getInstant(0), getInstant(+5), getActiveUser());

        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("fromTime", getTimestamp(+1));
        params.add("toTime", getTimestamp(+10));
        var response = requestMocker.getEntities(params);
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertFalse(responseEntities.contains(mapper.domainToPublic(entity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAdSpaceById() throws Exception {
        var entity = getSampleAdSpace();
        service.save(entity);

        var response = requestMocker.getEntity(entity.getId());
        var responseEntity = jsonMapper.parse(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertEquals(mapper.domainToPublic(entity), responseEntity);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testAddAdSpace() throws Exception {
        var newEntity = getSampleAdSpace();
        ctx.getBean(AdLocationService.class).save(newEntity.getAdLocation());
        var publicNewEntity = mapper.domainToPublic(newEntity);

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(e -> mapper.domainToPublic(e).equals(publicNewEntity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testUpdateAdSpaceImage() throws Exception {
        var entity = getSampleAdSpace();
        service.save(entity);
        byte[] imageBytes = getRandomBytes(1000);
        String imageFileName = getRandomString(10) + ".png";
        MockMultipartFile multipartFile = new MockMultipartFile("file", imageFileName, "", imageBytes);

        var response = requestMocker.postMultipartTo(multipartFile, "/" + entity.getId() + "/image");

        assertEquals(200, response.getStatus());
        assertEquals(imageFileName, service.findById(entity.getId()).orElseThrow().getImageFileNameWithExtension());
        assertEquals(imageBytes, service.findById(entity.getId()).orElseThrow().getImage());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void getAdSpaceImage() throws Exception {
        var entity = getSampleAdSpace();
        service.save(entity);
        byte[] imageBytes = getRandomBytes(1000);
        entity.setImage(imageBytes);

        var response = requestMocker.getFrom(entity.getId() + "/image");

        assertEquals(200, response.getStatus());
        assertEquals(MediaType.APPLICATION_OCTET_STREAM_VALUE, response.getContentType());
        assertArrayEquals(imageBytes, response.getContentAsByteArray());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testUpdateAdSpace() throws Exception {
        var entity = getSampleAdSpace();
        service.save(entity);
        var publicEntity = mapper.domainToPublic(entity);
        publicEntity.setLocator(getRandomString(5));

        var response = requestMocker.putEntity(entity.getId(), publicEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(e -> mapper.domainToPublic(e).equals(publicEntity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testDeleteAdSpace() throws Exception {
        var entity = getSampleAdSpace();
        service.save(entity);

        var response = requestMocker.deleteEntity(entity.getId());

        assertEquals(204, response.getStatus());
        assertFalse(service.findAll().contains(entity));
    }

    @Test
    @WithMockUser(username = "user")
    public void testAccessControl() throws Exception {
        var response = requestMocker.getEntities();

        assertEquals(401, response.getStatus());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testValidation() throws Exception {
        var newEntity = getSampleAdSpace();
        var publicNewEntity = mapper.domainToPublic(newEntity);
        publicNewEntity.setAdLocationId(null);

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(400, response.getStatus());
    }
}
