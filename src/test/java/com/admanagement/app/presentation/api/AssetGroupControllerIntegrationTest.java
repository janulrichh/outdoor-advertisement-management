package com.admanagement.app.presentation.api;

import com.admanagement.app.helpers.JsonMapper;
import com.admanagement.app.helpers.RequestMocker;
import com.admanagement.app.presentation.dtos.PublicAssetGroup;
import com.admanagement.app.presentation.mappers.AssetGroupMapper;
import com.admanagement.app.service.services.AssetGroupService;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.stream.Stream;

import static com.admanagement.app.helpers.TestConstants.ASSETGROUP_CONTROLLER_MAPPING;
import static com.admanagement.app.helpers.TestDataGenerator.getRandomString;
import static com.admanagement.app.helpers.TestDataGenerator.getSampleAssetGroup;
import static org.junit.jupiter.api.Assertions.*;

public class AssetGroupControllerIntegrationTest extends BaseEntityControllerIntegrationTest<PublicAssetGroup> {

    @Autowired
    private AssetGroupService service;
    @Autowired
    private AssetGroupMapper mapper;

    @BeforeAll
    protected void setup() {
        jsonMapper = new JsonMapper<>(new TypeReference<PublicAssetGroup>() {}, new TypeReference<List<PublicAssetGroup>>() {});
        requestMocker = new RequestMocker(ASSETGROUP_CONTROLLER_MAPPING, mockMvc);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAllAssetGroups() throws Exception {
        var entity1 = getSampleAssetGroup();
        var entity2 = getSampleAssetGroup();
        service.save(entity1);
        service.save(entity2);

        var response = requestMocker.getEntities();
        var responseEntities = jsonMapper.parseList(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertTrue(responseEntities
                .containsAll(Stream.of(entity1, entity2).map(e -> mapper.domainToPublic(e)).toList()));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testGetAssetGroupById() throws Exception {
        var entity = getSampleAssetGroup();
        service.save(entity);

        var response = requestMocker.getEntity(entity.getId());
        var responseEntity = jsonMapper.parse(response.getContentAsString());

        assertEquals(200, response.getStatus());
        assertEquals(mapper.domainToPublic(entity), responseEntity);
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testUpdateAssetGroup() throws Exception {
        var entity = getSampleAssetGroup();
        service.save(entity);
        var publicEntity = mapper.domainToPublic(entity);
        publicEntity.setName(getRandomString(10));

        var response = requestMocker.putEntity(entity.getId(), publicEntity);

        assertEquals(200, response.getStatus());
        assertTrue(service.findAll().stream().anyMatch(e -> mapper.domainToPublic(e).equals(publicEntity)));
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testDeleteAssetGroup() throws Exception {
        var entity = getSampleAssetGroup();
        service.save(entity);

        var response = requestMocker.deleteEntity(entity.getId());

        assertEquals(204, response.getStatus());
        assertFalse(service.findAll().contains(entity));
    }

    @Test
    @WithMockUser(username = "user")
    public void testAccessControl() throws Exception {
        var response = requestMocker.getEntities();

        assertEquals(401, response.getStatus());
    }

    @Test
    @WithMockUser(username = "sales", authorities = {"ROLE_SALES"})
    public void testValidation() throws Exception {
        var newEntity = getSampleAssetGroup();
        var publicNewEntity = mapper.domainToPublic(newEntity);
        publicNewEntity.setName(getRandomString(300));

        var response = requestMocker.postEntity(publicNewEntity);

        assertEquals(400, response.getStatus());
    }
}
