# Outdoor Advertisement Management

This repository contains the source code of Outdoor Advertisement Management System Back-End application,
which was developed as part of a bachelor's thesis "Välireklaam haldussüsteemi tagarakenduse arendamine". 
The front-end application is developed separately.
This file provides the instructions for setting up and running the project, a list of used technologies and methodologies
and a concise summary of the thesis to provide some context. The application is deployed to an AWS virtual server
and can be accessed at http://16.16.32.48:8080.


### Setting up
**1. Clone the repository**
```shell
git clone git@gitlab.com:janulrichh/outdoor-advertisement-management.git
```

**2. Run development and testing databases in Docker containers**
```shell
docker-compose up -d
```

**3. Build the project using gradle-wrapper (JVM version: 21)**

_Note: In order to pass the tests, the testing database must be running, and e-mail service STMP configuration must be present in application-test.properties._
```shell
./gradlew clean build
```

**4. Run the application**
```shell
./gradlew bootRun
```

**5. Try the [Postman collection](https://app.getpostman.com/join-team?invite_code=31fa29734a89e4f8246b7c93afbc0973&target_code=8cfc99ebbf1c95c23876ce0e66926f94) or [OpenAPI UI](http://13.48.126.161:8080/swagger-ui.html) ([local](http://127.0.0.1:8080/swagger-ui.html)) for usage examples**


### Technical details
This project demonstrates the use of the following methodologies and technologies (some more than others):
* Java 21, Spring Framework
* relational database, PostgreSQL 16
* geospatial data, PostGIS
* object-relational mapping
* SQL & JPQL query languages
* DB version control with Flyway
* JWT authentication
* automated testing
* Gradle
* Docker
* Amazon Web Services
* OpenAPI documentation
* 3-layered logical architecture
* REST architectural style
* repository pattern
* DTOs & mappers
* dependency injection
* aspect-oriented programming


### Thesis abstract
Outdoor advertising, also known as out-of-home (OOH) advertising, is a form of advertising that encompasses advertisement exposure on various public mediums, such as billboards, banners, public transportation vehicles and street furniture, which is intended to target consumers while they are away from their residences.

In the context of this thesis, a back-end application of outdoor advertisement management system is proposed, analyzed, and developed for an outdoor advertising services company, serving as a replacement for the existing spreadsheet-based solution.

The aim of this thesis is to analyze the proposed solution and create the server-side component of a distributed application, which will later be integrated with user interface, while ensuring compliance with company-approved requirements.

The paper provides an overview of the problem, analyzes existing solutions, presents the proposed solution and the chosen methodology and assesses functional and non-functional requirements. Subsequently, conducts a diverse analysis of relevant technological aspects, including data model design, selection of technologies and tools, security measures and architectural considerations. The development process is documented, and the results evaluated, while offering guidelines for applying the results and exploring possibilities for further development.

As a result, a back-end application is being developed and tested in Java following the waterfall approach. The application utilizes a database with spatial data capability and possesses application programming interface documentation compliant with the OpenAPI standard, thereby enabling user interface integration. In conclusion, the aim is achieved, and the application is brought into compliance with the requirements established during the analysis. In conjunction, potential applications for the outcomes are identified.

_(Full text in Estonian is published in TalTech digikogu)_


### Visuals

**Relation diagram of the source code modules**

![layers.png](visuals%2Flayers.png)

**Entity-Relationship Diagram** of the physical data model. Normalized to 3NF, standardized data types of SQL:2016.

![erd.png](visuals%2Ferd.png)

_(Other relevant visuals can be found in the thesis and its appendices)_
