FROM openjdk:21-jdk
ARG JAR_FILE=build/libs/app-1.0.0.jar
COPY ${JAR_FILE} app-1.0.0.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app-1.0.0.jar"]
